<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * add http to url
 * 
 * @param string $url
 * @return string
 */
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/**
 * friendly_url()
 * For friendly url
 * @param string $input
 */
function smart_url($input) {
    return preg_replace("/[^A-Za-z0-9]/", "", str_replace(" ", "", strip_tags($input)));
}

/**
 *
 * @param string $image_path
 * @return boolean or array
 *
 */
function image_attributes($image_path) {

	$file_headers = @get_headers($image_path);
	if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
		return false;
	}
	else {
		list($width, $height, $type, $attr) = getimagesize($image_path);
		return array(
				'width' => $width,
				'height' => $height,
				'type' => $type,
				'attr' => $attr
		);
	}
}

/**
 * 
 * @param unknown_type $url
 * @return string
 */
function image($url) {
    return '<img alt="" src="'.$url.'">';
}


?>
