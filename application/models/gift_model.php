<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gift_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->table = 'gift';
    }

    // --------------------------------------------------------------------

    /**
     * function save_gift()
     *
     * insert form data
     * @param $form_data - array
     * @return Bool - TRUE or FALSE
     */
    function save_gift($form_data) {
        $this->db->insert($this->table, $form_data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function update_gift($form_data, $id) {
        $this->db->update($this->table, $form_data, array('id' => $id));
        return TRUE;
    }

    function get_gift($id) {
        $query = $this->db->get_where($this->table, array('id' => $id));
        foreach ($query->result() as $row) {
            return $row;
        }
    }

    function user_has_gifts($gift_id) {

        $this->db->select('*');
        $this->db->from('gift');
        $this->db->join('user_has_gift', 'user_has_gift.gift_id = gift.id');
        $this->db->where('user_has_gift.gift_id', $gift_id);
        $query = $this->db->get();
        return $query->result();
    }

    function view_gifts() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_gift($gift_id) {
        $this->db->delete('user_has_gift', array('gift_id' => $gift_id));
        $this->db->delete($this->table, array('id' => $gift_id));
        return TRUE;
    }

    function suggested_gift($user_id, $gift_id) {

        $this->db->select('*');
        $this->db->from('user_has_gift');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {

            $data = array(
                'user_id' => $user_id,
                'gift_id' => $gift_id
            );
            $this->db->insert('user_has_gift', $data);
        } else {

            $data = array(
                'user_id' => $user_id,
                'gift_id' => $gift_id
            );

            $this->db->where('user_id', $user_id);
            $this->db->update('user_has_gift', $data);
        }
    }

}

?>
