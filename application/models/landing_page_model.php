<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of page_model
 *
 * @author rotich
 */
class Landing_Page_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function save_landing_page($form_data) {
        $this->db->insert('landing_page', $form_data); 
    }
    
    function edit_landing_page($form_data, $page_id) {
        $this->db->where('id', $page_id);
        $this->db->update('landing_page', $form_data);
    }
    
    function delete_landing_page($page_id) {
        $this->db->delete('landing_page', array('id' => $page_id));
    }
    
    function view_landing_page($page_id) {

        $this->db->select('*');
        $this->db->from('landing_page');
        $this->db->where('id', $page_id);
        $query = $this->db->get();
        foreach ($query->result() as $row) {
            return $row;
        }
    }

}

?>
