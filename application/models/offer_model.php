<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Offer_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->table = 'offer';
        $this->user_has_offer = 'user_has_offer';
    }

    function save_offer($form_data) {
        $this->db->insert($this->table, $form_data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function update_offer($form_data, $id) {
        $this->db->update($this->table, $form_data, array('id' => $id));
        return TRUE;
    }

    function get_offer($id) {
        $query = $this->db->get_where($this->table, array('id' => $id));
        foreach ($query->result() as $row) {
            return $row;
        }
    }

    function view_offers() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_offer($id) {
        $this->db->delete($this->table, array('id' => $id));
        return TRUE;
    }

    function save_click($user_id, $offer_id) {

        $insert_array = array(
            'user_id' => $user_id,
            'offer_id' => $offer_id,
            'created' => date('Y-m-d H:i:s')
        );

        $update_array = array(
            'created' => date('Y-m-d H:i:s')
        );

        $select_record = array(
            'user_id' => $user_id,
            'offer_id' => $offer_id,
            'success' => 0
        );

        $query = $this->db->get_where($this->user_has_offer, $select_record);

        if ($query->num_rows() == 0) {
            $this->db->insert($this->user_has_offer, $insert_array);
        } else {
            $this->db->where($select_record);
            $this->db->update($this->user_has_offer, $update_array);
        }


        if ($this->db->affected_rows() == '1') {
            $this->session->unset_userdata('selected_gift_id');
            return TRUE;
        }

        return FALSE;
    }

}

?>
