<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users extends CI_Model {

    private $table_name = 'user';   // user accounts
    private $profile_table_name = 'user_profile'; // user profiles
    private $offers_table_name = 'offer'; // offer
    private $user_has_offer_table_name = 'user_has_offer'; // user has offer

    function __construct() {
        parent::__construct();

        $ci = & get_instance();
        $this->table_name = $ci->config->item('db_table_prefix', 'tank_auth') . $this->table_name;
        $this->profile_table_name = $ci->config->item('db_table_prefix', 'tank_auth') . $this->profile_table_name;
        $this->offers_table_name = $ci->config->item('db_table_prefix', 'tank_auth') . $this->offers_table_name;
        $this->user_has_offer_table_name = $ci->config->item('db_table_prefix', 'tank_auth') . $this->user_has_offer_table_name;
    }

    /**
     * Get user record by Id
     *
     * @param	int
     * @param	bool
     * @return	object
     */
    function get_user_by_id($user_id, $activated) {
        $this->db->where('id', $user_id);
        $this->db->where('activated', $activated ? 1 : 0);

        $query = $this->db->get($this->table_name);
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Get user record by login (username or email)
     *
     * @param	string
     * @return	object
     */
    function get_user_by_login($login) {
        $this->db->select($this->table_name . '.*');
        $this->db->select('user_profile.is_admin');
        $this->db->from($this->table_name);
        $this->db->where('LOWER(user.username)=', strtolower($login));
        $this->db->or_where('LOWER(user.email)=', strtolower($login));
        $this->db->join('user_profile', 'user_profile.user_id = user.id');
        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Get user record by username
     *
     * @param	string
     * @return	object
     */
    function get_user_by_username($username) {
        $this->db->select($this->table_name . '.*');
        $this->db->select('user_profile.is_admin');
        $this->db->from($this->table_name);
        $this->db->where('LOWER(user.username)=', strtolower($username));
        $this->db->join('user_profile', 'user_profile.user_id = user.id');
        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Get user record by email
     *
     * @param	string
     * @return	object
     */
    function get_user_by_email($email) {
        $this->db->select($this->table_name . '.*');
        $this->db->select('user_profile.is_admin');
        $this->db->from($this->table_name);
        $this->db->where('LOWER(user.email)=', strtolower($email));
        $this->db->join('user_profile', 'user_profile.user_id = user.id');
        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Check if username available for registering
     *
     * @param	string
     * @return	bool
     */
    function is_username_available($username) {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(username)=', strtolower($username));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
     * Check if email available for registering
     *
     * @param	string
     * @return	bool
     */
    function is_email_available($email) {
        $this->db->select('1', FALSE);
        $this->db->where('LOWER(email)=', strtolower($email));
        $this->db->or_where('LOWER(new_email)=', strtolower($email));

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 0;
    }

    /**
     * Create new user record
     *
     * @param	array
     * @param	bool
     * @return	array
     */
    function create_user($data, $activated = TRUE) {

        $data['created'] = date('Y-m-d H:i:s');
        $data['activated'] = $activated ? 1 : 0;

        if ($this->db->insert($this->table_name, $data)) {
            $user_id = $this->db->insert_id();
            $this->create_profile($user_id);
            return array('user_id' => $user_id);
        }
        return NULL;
    }

    /**
     * Activate user if activation key is valid.
     * Can be called for not activated users only.
     *
     * @param	int
     * @param	string
     * @param	bool
     * @return	bool
     */
    function activate_user($user_id, $activation_key, $activate_by_email) {
        $this->db->select('1', FALSE);
        $this->db->where('id', $user_id);
        if ($activate_by_email) {
            $this->db->where('new_email_key', $activation_key);
        } else {
            $this->db->where('new_password_key', $activation_key);
        }
        $this->db->where('activated', 0);
        $query = $this->db->get($this->table_name);

        if ($query->num_rows() == 1) {

            $this->db->set('activated', 1);
            $this->db->set('new_email_key', NULL);
            $this->db->where('id', $user_id);
            $this->db->update($this->table_name);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Purge table of non-activated users
     *
     * @param	int
     * @return	void
     */
    function purge_na($expire_period = 172800) {
        $this->db->where('activated', 0);
        $this->db->where('UNIX_TIMESTAMP(created) <', time() - $expire_period);
        $this->db->delete($this->table_name);
    }

    /**
     * Delete user record
     *
     * @param	int
     * @return	bool
     */
    function delete_user($user_id) {
        $this->db->where('id', $user_id);
        $this->db->delete($this->table_name);
        if ($this->db->affected_rows() > 0) {
            $this->delete_profile($user_id);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Set new password key for user.
     * This key can be used for authentication when resetting user's password.
     *
     * @param	int
     * @param	string
     * @return	bool
     */
    function set_password_key($user_id, $new_pass_key) {
        $this->db->set('new_password_key', $new_pass_key);
        $this->db->set('new_password_requested', date('Y-m-d H:i:s'));
        $this->db->where('id', $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Check if given password key is valid and user is authenticated.
     *
     * @param	int
     * @param	string
     * @param	int
     * @return	void
     */
    function can_reset_password($user_id, $new_pass_key, $expire_period = 900) {
        $this->db->select('1', FALSE);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $new_pass_key);
        $this->db->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);

        $query = $this->db->get($this->table_name);
        return $query->num_rows() == 1;
    }

    /**
     * Change user password if password key is valid and user is authenticated.
     *
     * @param	int
     * @param	string
     * @param	string
     * @param	int
     * @return	bool
     */
    function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900) {
        $this->db->set('password', $new_pass);
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $new_pass_key);
        $this->db->where('UNIX_TIMESTAMP(new_password_requested) >=', time() - $expire_period);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Change user password
     *
     * @param	int
     * @param	string
     * @return	bool
     */
    function change_password($user_id, $new_pass) {
        $this->db->set('password', $new_pass);
        $this->db->where('id', $user_id);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Set new email for user (may be activated or not).
     * The new email cannot be used for login or notification before it is activated.
     *
     * @param	int
     * @param	string
     * @param	string
     * @param	bool
     * @return	bool
     */
    function set_new_email($user_id, $new_email, $new_email_key, $activated) {
        $this->db->set($activated ? 'new_email' : 'email', $new_email);
        $this->db->set('new_email_key', $new_email_key);
        $this->db->where('id', $user_id);
        $this->db->where('activated', $activated ? 1 : 0);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Activate new email (replace old email with new one) if activation key is valid.
     *
     * @param	int
     * @param	string
     * @return	bool
     */
    function activate_new_email($user_id, $new_email_key) {
        $this->db->set('email', 'new_email', FALSE);
        $this->db->set('new_email', NULL);
        $this->db->set('new_email_key', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_email_key', $new_email_key);

        $this->db->update($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    /**
     * Update user login info, such as IP-address or login time, and
     * clear previously generated (but not activated) passwords.
     *
     * @param	int
     * @param	bool
     * @param	bool
     * @return	void
     */
    function update_login_info($user_id, $record_ip, $record_time) {
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);

        if ($record_ip)
            $this->db->set('last_ip', $this->input->ip_address());
        if ($record_time)
            $this->db->set('last_login', date('Y-m-d H:i:s'));

        $this->db->where('id', $user_id);
        $this->db->update($this->table_name);
    }

    /**
     * Ban user
     *
     * @param	int
     * @param	string
     * @return	void
     */
    function ban_user($user_id, $reason = NULL) {
        $this->db->where('id', $user_id);
        $this->db->update($this->table_name, array(
            'banned' => 1,
            'ban_reason' => $reason,
        ));
    }

    /**
     * Unban user
     *
     * @param	int
     * @return	void
     */
    function unban_user($user_id) {
        $this->db->where('id', $user_id);
        $this->db->update($this->table_name, array(
            'banned' => 0,
            'ban_reason' => NULL,
        ));
    }

    /**
     * Create an empty profile for a new user
     *
     * @param	int
     * @return	bool
     */
    private function create_profile($user_id) {
        $this->db->set('user_id', $user_id);
        return $this->db->insert($this->profile_table_name);
    }

    /**
     * Delete user profile
     *
     * @param	int
     * @return	void
     */
    private function delete_profile($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->delete($this->profile_table_name);
    }

    /**
     *
     * Update user profile
     * @param int $user_id
     * @param array $user_data
     */
    function update_user_profile($user_id, $user_data) {

        $this->db->where('user_id', $user_id);
        $this->db->update($this->profile_table_name, $user_data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 
     * @param int $user_id
     * @return array
     */
    function get_user_profile($user_id) {

        $this->db->where('user_id', $user_id);
        $query = $this->db->get($this->profile_table_name);

        if ($query->num_rows() == 1) {
            return $query->row();
        }
        return NULL;
    }

    function user_referrals($user_id) {

        $this->db->select('*');
        $this->db->select('(SELECT COUNT(*) FROM user_has_offer WHERE user_has_offer.user_id = `up`.user_id and user_has_offer.success = 1) as completed');
        $this->db->from('user as u');
        $this->db->join('user_profile as up', 'u.id = up.user_id');
        $this->db->where('up.referrer', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    function user_offers($user_id) {

        $this->db->select('offer.*');
        $this->db->select('gift.image as gift_image, gift.name as gift_name');
        $this->db->select('user_has_offer.success, user_has_offer.user_id, user_has_offer.offer_id');
        $this->db->from('offer');
        $this->db->join('user_has_offer', 'user_has_offer.offer_id = offer.id');
        $this->db->join('gift', 'user_has_offer.gift_id = gift.id', 'left');
        $this->db->where('user_has_offer.user_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    function user_gift($user_id) {

        $this->db->select('*');
        $this->db->from('gift');
        $this->db->join('user_has_gift', 'user_has_gift.gift_id = gift.id');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        }
        return NULL;
    }

    function get_user_offer($user_id, $offer_id) {

        $this->db->select('*');
        $this->db->from($this->offers_table_name);
        $this->db->join($this->user_has_offer_table_name, $this->user_has_offer_table_name . '.offer_id = ' . $this->offers_table_name . '.id');
        $this->db->join($this->profile_table_name, $this->profile_table_name . '.user_id = ' . $this->user_has_offer_table_name . '.user_id');
        $this->db->where($this->user_has_offer_table_name . '.user_id', $user_id);
        $this->db->where($this->user_has_offer_table_name . '.offer_id', $offer_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        }
        return NULL;
    }

    function edit_user_offer($user_id, $offer_id, $update_array) {

        $this->db->where('user_id', $user_id);
        $this->db->where('offer_id', $offer_id);
        $this->db->update($this->user_has_offer_table_name, $update_array);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function user_referrer($referrer, $user_id) {

        $this->db->select('up.user_id as u_id, up.name as u_name');
        $this->db->select('ur.user_id as r_id, ur.name as r_name');
        $this->db->from('user_profile as up');
        $this->db->join('user_profile as ur', 'ur.user_id = up.referrer');
        $this->db->where('up.referrer', $referrer);
        $this->db->where('up.user_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        }
        return NULL;
    }

    function save_referrer_gift($insert_array) {

        $this->db->insert('referrer_gift', $insert_array);
        if ($this->db->affected_rows() == 1) {
            return TRUE;
        }
        return FALSE;
    }

    function referrer_gifts($user_id) {
        $this->db->select('*');
        $this->db->from('gift');
        $this->db->join('referrer_gift', 'referrer_gift.gift_id = gift.id');
        $this->db->join('user', 'user.id = referrer_gift.referrer');
        $this->db->where('user.id', $user_id);
        $this->db->group_by("gift.id");
        $query = $this->db->get();
        return $query->result();
    }

    function offer_gifts($user_id) {
        $this->db->select('*');
        $this->db->from('gift');
        $this->db->join('user_has_offer', 'user_has_offer.gift_id = gift.id');
        $this->db->where('user_has_offer.user_id', $user_id);
        $this->db->group_by("gift.id");
        $query = $this->db->get();
        return $query->result();
    }

}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */