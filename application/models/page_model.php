<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of page_model
 *
 * @author rotich
 */
class Page_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function view($slug) {

        $this->db->select('*');
        $this->db->from('page');
        $this->db->where('slug', $slug);
        $query = $this->db->get();
        foreach ($query->result() as $row) {
            return $row;
        }
    }

    function edit_page($form_data, $slug) {
        $this->db->where('slug', $slug);
        $this->db->update('page', $form_data);
    }

}

?>
