<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function GetMainLinks() {
        $query = $this->db->get('main_link');
        $links_array = '';
        $c = 0;
        foreach ($query->result() as $row) {
            $links_array[$c]['title'] = $row->title;
            $links_array[$c]['url'] = $row->url;
            $c++;
        }
        return $links_array;
    }

}

?>
