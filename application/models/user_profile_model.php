<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_profile_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->table = 'user_profile';
    }

    // --------------------------------------------------------------------

    /**
     * function SaveForm()
     *
     * insert form data
     * @param $form_data - array
     * @return Bool - TRUE or FALSE
     */
    function SaveUser_profile($form_data) {
        $this->db->insert($this->table, $form_data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function UpdateUser_profile($form_data, $id) {
        $this->db->update($this->table, $form_data, array('id' => $id));
        return TRUE;
    }

    function GetUser_profileFields() {
        $fields_array = array('Profile ID', 'User', 'Is admin?', 'First name', 'Second name', 'Address', 'City', 'Postal Code', 'County', 'Country', 'Website', 'Referrer', 'Referral Status');
        return $fields_array;
    }

    function GetUser_profiles() {
        $query = $this->db->get($this->table);

        $item_array = '';
        $c = 0;
        foreach ($query->result() as $row) {
            $item_array[$c]['id'] = $row->id;
            $item_array[$c]['user_id'] = $row->user_id;
            $item_array[$c]['is_admin'] = $row->is_admin;
            $item_array[$c]['first_name'] = $row->first_name;
            $item_array[$c]['second_name'] = $row->second_name;
            $item_array[$c]['address'] = $row->address;
            $item_array[$c]['city'] = $row->city;
            $item_array[$c]['postal_code'] = $row->postal_code;
            $item_array[$c]['county'] = $row->county;
            $item_array[$c]['country'] = $row->country;
            $item_array[$c]['website'] = $row->website;
            $item_array[$c]['referrer'] = $row->referrer;
            $item_array[$c]['refer_status'] = $row->refer_status;
            $c++;
        }

        return $item_array;
    }

    function GetUser_profile($id) {
        $query = $this->db->get_where($this->table, array('id' => $id));
        $item_array = '';
        foreach ($query->result() as $row) {
            $item_array['id'] = $row->id;
            $item_array['user_id'] = $row->user_id;
            $item_array['is_admin'] = $row->is_admin;
            $item_array['first_name'] = $row->first_name;
            $item_array['second_name'] = $row->second_name;
            $item_array['address'] = $row->address;
            $item_array['city'] = $row->city;
            $item_array['postal_code'] = $row->postal_code;
            $item_array['county'] = $row->county;
            $item_array['country'] = $row->country;
            $item_array['website'] = $row->website;
            $item_array['referrer'] = $row->referrer;
            $item_array['refer_status'] = $row->refer_status;
        }
        return $item_array;
    }

    function GetUsers() {
        $query = $this->db->get('user');

        $item_array = '';
        foreach ($query->result() as $row) {
            $item_array[$row->id] = $row->username;
        }

        return $item_array;
    }

    function GetUser_profileStatus($id) {
        $this->db->select("*");
        $this->db->from('status');
        $this->db->where('id', $id);
        $query = $this->db->get();

        $item = '';
        foreach ($query->result() as $row) {
            $item = $row->title;
        }

        return $item;
    }

    function GetUser_profileStatuses() {
        $this->db->select("*");
        $this->db->from('status');
        $query = $this->db->get();

        $item = '';
        foreach ($query->result() as $row) {
            $item[$row->id] = $row->title;
        }

        return $item;
    }

    function DeleteUser_profile($id) {
        $this->db->delete($this->table, array('id' => $id));
        return TRUE;
    }

}

?>
