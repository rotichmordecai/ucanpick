<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->table = 'category';
    }

    // --------------------------------------------------------------------

    /**
     * function SaveForm()
     *
     * insert form data
     * @param $form_data - array
     * @return Bool - TRUE or FALSE
     */
    function SaveCategory($form_data) {
        $this->db->insert($this->table, $form_data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function UpdateCategory($form_data, $id) {
        $this->db->update($this->table, $form_data, array('id' => $id));
        return TRUE;
    }

    function GetCategoryFields() {
        $fields_array = array('Category ID', 'Title', 'Description', 'Status');
        return $fields_array;
    }

    function GetCategories() {
        $query = $this->db->get($this->table);

        $item_array = '';
        $c = 0;
        foreach ($query->result() as $row) {
            $item_array[$c]['id'] = $row->id;
            $item_array[$c]['title'] = $row->title;
            $item_array[$c]['description'] = $row->description;
            $item_array[$c]['active'] = $this->GetCategoryStatus($row->active);
            $c++;
        }

        return $item_array;
    }

    function GetCategory($id) {
        $query = $this->db->get_where($this->table, array('id' => $id));
        $item_array = '';
        foreach ($query->result() as $row) {
            $item_array['id'] = $row->id;
            $item_array['title'] = $row->title;
            $item_array['description'] = $row->description;
            $item_array['active'] = $row->active;
        }
        return $item_array;
    }

    function GetCategoryStatus($id) {
        $this->db->select("*");
        $this->db->from('status');
        $this->db->where('id', $id);
        $query = $this->db->get();

        $item = '';
        foreach ($query->result() as $row) {
            $item = $row->title;
        }

        return $item;
    }

    function GetCategoryStatuses() {
        $this->db->select("*");
        $this->db->from('status');
        $query = $this->db->get();

        $item = '';
        foreach ($query->result() as $row) {
            $item[$row->id] = $row->title;
        }

        return $item;
    }

    function DeleteCategory($id) {
        $this->db->delete($this->table, array('id' => $id));
        return TRUE;
    }

}

?>
