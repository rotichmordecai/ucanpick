<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author rotich
 */
class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    function load_single_column() {
        $this->template->set_template('single_column');
        $this->template->add_css('assets/css/custom.css');
        $this->template->add_css('assets/css/jquery.lightbox-0.5.css');
        $this->template->add_css('assets/css/jquery.toastmessage-min.css');
        $this->template->add_css('body { padding-top: 0px; padding-bottom: 40px; }', 'embed', 'print');
        $this->template->add_js('assets/js/jquery-1.8.1.min.js');
        $this->template->add_js('assets/js/bootstrap.min.js');
        $this->template->add_js('assets/js/jquery.lightbox-0.5.js');
        $this->template->add_js('assets/js/jquery.toastmessage-min.js');
        $this->template->add_js('assets/js/application.js');
        $this->template->add_js('assets/js/jquery.placeholder.js');
    }
    
    function load_landing_page() {
        $this->template->set_template('landing_page');
        $this->template->add_css('assets/css/custom.css');
        $this->template->add_css('assets/css/jquery.lightbox-0.5.css');
        $this->template->add_css('assets/css/jquery.toastmessage-min.css');
        $this->template->add_css('body { padding-top: 0px; padding-bottom: 40px; }', 'embed', 'print');
        $this->template->add_js('assets/js/jquery-1.8.1.min.js');
        $this->template->add_js('assets/js/bootstrap.min.js');
        $this->template->add_js('assets/js/jquery.lightbox-0.5.js');
        $this->template->add_js('assets/js/jquery.toastmessage-min.js');
        $this->template->add_js('assets/js/application.js');
        $this->template->add_js('assets/js/jquery.placeholder.js');
    }

}

?>
