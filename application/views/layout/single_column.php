
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>UCanPick.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->

        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic|PT+Sans+Narrow:400,700|PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
        <?php echo $_styles; ?>


        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    </head>

    <body>
        <div class="wrap">
            <div class="header">
                <?php
                $image_properties = array(
                    'src' => 'assets/img/logo.png',
                    'alt' => 'Ucanpick.com',
                    'class' => 'logo',
                    'title' => 'Ucanpick.com',
                    'rel' => '',
                );
                echo anchor('', img($image_properties), 'title="Youcanpick.com"');
                ?>
                <form action="<?php echo site_url("auth/login"); ?>" class="login" method="post" style="margin-top: 43px;">
                    <?php if (!$this->tank_auth->is_logged_in()): ?>
                        <input type="text" name="login" id="login" placeholder="Email" class="input-medium pull-right" style="margin-bottom: 1px; clear: both;">
                        <input type="password" name="password" id="password" placeholder="Password" class="input-medium pull-right" style="margin-bottom: 1px; clear: both;">
                        <?php
                        $data = array(
                            'name' => 'submit',
                            'id' => 'submit',
                            'value' => 'true',
                            'type' => 'submit',
                            'content' => 'Login',
                            'class' => 'login-button'
                        );

                        echo form_button($data);
                        ?>
                        <label class="forgot-password"><?php echo anchor('/auth/forgot_password/', 'Forgot Password ?'); ?></label>
                    <?php else: ?>
                        <div style="float: right; margin-top: 67px;">
                            <strong><?php echo $this->tank_auth->get_username(); ?></strong>Logged in : <?php echo anchor('/auth/logout/', 'Logout !'); ?>
                        </div>
                    <?php endif; ?>
                </form>
                <ul class="navigation">
                    <li>
                        <?php echo anchor('about', 'How Does it Work?', array('title' => 'About Us!')); ?>
                    </li>
                    <li>
                        <?php echo anchor('faqs', 'FAQs', array('title' => 'FAQs!')); ?>
                    </li>
                    <li>
                        <?php echo anchor('testimonials', 'Testimonials', array('title' => 'Testimonials!')); ?>
                    </li>
                    <li>
                        <?php echo anchor('contactus', 'Contact Us', array('title' => 'Contact Us!')); ?>
                    </li>
                </ul>
            </div>
            <!-- Example row of columns -->
            <div class="middle">
                <div class="span12">
                    <?php if ($this->tank_auth->is_logged_in()): ?>
                        <?php $this->load->view('user/navigation'); ?>
                    <?php endif; ?>
                    <div class="page-title"><?php echo isset($title) ? $title : ''; ?></div>
                    <?php echo $content; ?>
                </div>
            </div>

            <div class="footer">
                <div class="c_footer">
                    <div class="footer-navigation">
                        <ul>
                            <li>
                                <?php echo anchor('terms', 'Terms & Conditions', array('title' => 'Terms & Conditions!')); ?>
                            </li>
                            <li>
                                <?php echo anchor('privacy', 'Privacy Policy', array('title' => 'Privacy Policy!')); ?>
                            </li>
                            <li>&nbsp;&nbsp;</li>
                            <li>
                                <a href="http://www.facebook.com/UCanPick" target="_blank"><?php echo img('assets/img/icons/facebook.png'); ?></a>
                                <a href="https://twitter.com/ucanpick" target="_blank"><?php echo img('assets/img/icons/twitter.png'); ?></a>
                            </li>
                        </ul>
                    </div>
                    <a href="//privacy-policy.truste.com/click-to-verify/www.ucanpick.com" title="Privacy Policy by TRUSTe" target="_blank" style="margin-left: 510px; clear: both; float: left;  margin-top: 25px;"><img style="border: none" alt="Privacy Policy by TRUSTe" src="//privacy-policy.truste.com/verified-seal/www.ucanpick.com/green/h.png"/></a>
                    <div class="copyright">
                        &copy; <?php echo date('Y'); ?> Turman North Inc., All rights reserved
                    </div>
                </div>
            </div>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <div id="messages" style="visibility: hidden;">
            <?php
            $message = $this->session->flashdata('message');
            echo $message;
            ?>
        </div>

        <?php echo $_scripts; ?>

        <script type="text/javascript">
            var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-35377576-1']); _gaq.push(['_trackPageview']);
            (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();
        </script>

    </body>
</html>
