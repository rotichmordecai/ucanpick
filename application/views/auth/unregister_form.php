<div class="c_hero-unit">
    <?php
    $password = array(
        'name' => 'password',
        'id' => 'password',
        'size' => 30,
    );
    ?>
    <?php
    $attributes = array('class' => 'middle-login', 'id' => '');
    echo form_open($this->uri->uri_string(), $attributes);
    ?>
    <table>
        <tr>
            <td><?php echo form_label('Password', $password['id']); ?></td>
            <td><?php echo form_password($password); ?></td>
            <td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']]) ? $errors[$password['name']] : ''; ?></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <?php
                $data = array(
                    'name' => 'cancel',
                    'id' => 'cancel',
                    'value' => 'true',
                    'type' => 'submit',
                    'content' => 'Delete account',
                    'class' => 'submit-button'
                );
                echo form_button($data);
                ?>
            </td>
            <td style="color: red;"></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>