<div class="c_hero-unit">
    <?php
    $login = array(
        'name' => 'login',
        'id' => 'login',
        'value' => set_value('login'),
        'maxlength' => 80,
        'size' => 30,
    );
    if ($this->config->item('use_username', 'tank_auth')) {
        $login_label = 'Email or login';
    } else {
        $login_label = 'Email';
    }
    ?>
    <?php
    $attributes = array('class' => 'middle-login', 'id' => '');
    echo form_open($this->uri->uri_string(), $attributes);
    ?>
    <table>
        <tr>
            <td><?php echo form_label($login_label, $login['id']); ?></td>
            <td><?php echo form_input($login); ?></td>
            <td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']]) ? $errors[$login['name']] : ''; ?></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <?php
                $data = array(
                    'name' => 'reset',
                    'id' => 'reset',
                    'value' => 'true',
                    'type' => 'submit',
                    'content' => 'Get a new password',
                    'class' => 'submit-button'
                );
                echo form_button($data);
                ?>
            </td>
            <td style="color: red;"></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>