<!-- Main hero unit for a primary marketing message or call to action -->
<div class="c_hero-unit c_homepage">
    <div class="left-column">
        <h1>Great Offers.</h1>
        <h1>Free cash or Gifts .</h1>
        <h1>In three simple steps.</h1>
        <p>With u can pick, it is easy to earn free cash, a free ipad or free kindle by completing offers from one of our select advertising partners, and having your friends do the same </p>
        <p><a href="about" class="learn-more">Learn more</a></p>
        <br/>
        <div class="under-more"></div>
    </div>
    <div class="right-column">
        <div class="instruction-1">1. Sign up for an offer (Choose from Bank Accounts, Credit cards, and much more)</div>
        <div class="instruction-2">2. Refer friends to sign up <br />&nbsp; &nbsp; For offers as well</div>
        <div class="instruction-3">3. U can pick your free cash reward or gift</div>
    </div>
    <form action="<?php echo site_url("auth/register"); ?>" method="post" class="sign-up" >
    	<span style="float: left; width: 450px; color: #014E44; font-size: 20px; font-weight: 500; margin-top: 5px;">
    	FREE CASH. FREE IPADS. FREE KINDLES.
    	</span>
        <input type="text" name="email" class="email" placeholder="Enter your Email">
        <button type="submit" class="submit-button">SIGN UP NOW</button>
    </form>
</div>

<div class="c_gift-bar" id="gallery">
    <div class="gift-text">
        Awesome Gift Options &nbsp; <?php echo img('assets/img/arrow-right.png'); ?> &nbsp;
        Amazing Customer Service &nbsp; <?php echo img('assets/img/arrow-right.png'); ?> &nbsp;
        Your private information is never shared &nbsp; <?php echo img('assets/img/arrow-right.png'); ?> &nbsp;
        No Additional Fees or Delivery Charges
    </div>
    <div class="c_gift">
        <span style="display: block; font-size: 11px; font-weight: bold;">Cash</span>
        <a href="<?php echo base_url('images/Cash_5.png'); ?>" title="">
            <?php
            $image_attributes = image_attributes(base_url('images/Cash_5.png'));
            $width = 0;
            $height = 80;
            if ($image_attributes) {
            	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
            }
            echo img(array('src' => 'images/Cash_5.png', 'alt' => 'Image', 'class' => 'img-rounded', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => ''));
            ?>
        </a>
        <span style="display: block; font-size: 11px; font-weight: bold;">2 referrals</span>
    </div>
    <div class="c_gift">
        <span style="display: block; font-size: 11px; font-weight: bold;">Cash</span>
        <a href="<?php echo base_url('images/Cash.png'); ?>" title="">
            <?php
            $image_attributes = image_attributes(base_url('images/Cash.png'));
            $width = 0;
            $height = 80;
            if ($image_attributes) {
            	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
            }
            echo img(array('src' => 'images/Cash.png', 'alt' => 'Image', 'class' => 'img-rounded', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => ''));
            ?>
        </a>
        <span style="display: block; font-size: 11px; font-weight: bold;">4 referrals</span>
    </div>
    <div class="c_gift">
        <span style="display: block; font-size: 11px; font-weight: bold;">IPAD 2 16gb</span>
        <a href="<?php echo base_url('images/IPAD216GB1.png'); ?>" title="">
            <?php
            $image_attributes = image_attributes(base_url('images/IPAD216GB1.png'));
            $width = 0;
            $height = 80;
            if ($image_attributes) {
            	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
            }
            echo img(array('src' => 'images/IPAD216GB1.png', 'alt' => 'Image', 'class' => 'img-rounded', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => ''));
            ?>
        </a>
        <span style="display: block; font-size: 11px; font-weight: bold;">13 referrals</span>
    </div>
    <div class="c_gift">
        <span style="display: block; font-size: 11px; font-weight: bold;">Kindle HD 16gb 7 inch</span>
        <a href="<?php echo base_url('images/KindleFireHD16GB7Inch.jpg'); ?>" title="">
            <?php
            $image_attributes = image_attributes(base_url('images/KindleFireHD16GB7Inch.jpg'));
            $width = 0;
            $height = 80;
            if ($image_attributes) {
            	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
            }
            echo img(array('src' => 'images/KindleFireHD16GB7Inch.jpg', 'alt' => 'Image', 'class' => 'img-rounded', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => ''));
            ?>
        </a>
        <span style="display: block; font-size: 11px; font-weight: bold;">8 referrals</span>
    </div>
    <div class="c_gift">
        <span style="display: block; font-size: 11px; font-weight: bold;">Kindle HD 16gb 8.9 inch</span>
        <a href="<?php echo base_url('images/KindleFireHD16GB89Inch.jpg'); ?>" title="">
            <?php
            $image_attributes = image_attributes(base_url('images/KindleFireHD16GB89Inch.jpg'));
            $width = 0;
            $height = 80;
            if ($image_attributes) {
            	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
            }
            echo img(array('src' => 'images/KindleFireHD16GB89Inch.jpg', 'alt' => 'Image', 'class' => '', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => ''));
            ?>
        </a>
        <span style="display: block; font-size: 11px; font-weight: bold;">9 referrals</span>
    </div>
</div>