<div class="page-title">My Gift</div>
<div>
    <table style="margin: 15px;">
        <tr>
            <td width="45%">
                <p>Select a gift below or tell us what you want and we can customize a gift for you - U can pick!</p>
                <p> Just submit your idea to us and we'll tell you how many referrals you will need</p> 
            </td>
            <td width="10%">
                &nbsp;
            </td>
            <td width="45%">
                <p>
                    Enter your custom gift Idea :
                </p>
                <form action="" method="post">
                    <p>
                        <input type="text" name="suggestgift" class="suggestgift" placeholder="Custom Gift" />
                        <input type="submit" value="Send" class="suggestgift-button" />
                    </p>
                </form>
            </td>
        </tr>
    </table>
</div>
<?php if (is_object($gift)): ?>
    <div class="success">
        You have currently selected <span style="font-weight: bold;"><?php echo $gift->name; ?></span> Click on another item below to change your gift option
    </div>
<?php endif; ?>
<div class="c_offer-bar">
    <?php if (is_array($gifts)): ?>
        <?php foreach ($gifts as $gift): ?>
            <div class="c_offer">
            	<span style="font-weight: bold; font-size: 14px;"><?php echo anchor('gifts/click/' . $gift->id, $gift->name, array('title' => 'Select Gift!')); ?></span>
                <?php
                $image_attributes = image_attributes(base_url('images/' . $gift->image));
                $width = 0;
                $height = 130;
                if ($image_attributes) {
                	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
                }
                echo anchor('gifts/click/' . $gift->id, img(array('src' => 'images/' . $gift->image, 'alt' => 'Image', 'class' => 'img-rounded', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => '')), array('title' => 'Select Gift!')); 
                ?>
                <span style="font-weight: bold; font-size: 14px;"><?php echo $gift->referrals; ?> Referrals</span>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>