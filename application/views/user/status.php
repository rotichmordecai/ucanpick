<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$offers_needed = 0;
?>
<div style="overflow: hidden; float: none; text-align: center; padding: 20px;">
    <?php if (is_object($gift)): ?>
    <h1 style="font-size: 18px;"><?php echo $gift->name; ?></h1>
    <br />
        <?php
        $image_attributes = image_attributes(base_url('images/' . $gift->image));
        $width = 0;
        $height = 150;
        if ($image_attributes) {
        	$width = ($image_attributes['width']*$height)/$image_attributes['height'];
        } 
        echo is_file('images/' . $gift->image) ? img(array('src' => 'images/' . $gift->image, 'alt' => 'Image', 'class' => 'img-rounded', 'width' => $width, 'height' => $height, 'title' => 'Gift Image', 'rel' => '')) : ''; 
        ?>
        <br />
        <h1 style="font-size: 14px;">Complete <?php echo $gift->referrals; ?> referrals to earn this gift</h1>
        <br />
        <?php echo anchor('gifts', '[ Change Free Gift ]', array('title' => 'Change Free Gift!')); ?>
        <?php
        $offers_needed = $gift->referrals;
        ?>
    <?php endif; ?>
</div>

<?php $all_referrals = 0; ?>
<?php $completed_referrals = 0; ?>
<?php foreach ($referrals as $referral): ?>
    <?php $all_referrals = $all_referrals + 1; ?>
    <?php
    if ($referral->completed > 0) {
        $completed_referrals = $completed_referrals + 1;
    }
    ?>
<?php endforeach; ?>

<div class="success" style="text-align: center; height: 30px; font-size: 20px;">
    <?php $all_offers = 0; ?>
    <?php $completed_offers = 0; ?>
    <?php foreach ($offers as $offer): ?>
        <?php $all_offers = $all_offers + 1; ?>
        <?php
        if ($offer->success > 0) {
            $completed_offers = $completed_offers + 1;
        }
        ?>
    <?php endforeach; ?>
    <?php if ($completed_offers != 0): ?>
        Offer completed
    <?php else: ?>
        Offer not completed
    <?php endif; ?>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    <?php echo $completed_referrals; ?>/<?php echo $all_referrals; ?> referrals completed
</div>

<table cellpadding="0" cellspacing="0" border="0" class="my-status"  id="user_referrals" >
    <thead>
        <tr>
            <th width="">Name</th>
            <th width="">Id#</th>
            <th width="">Email Address</th>
            <th width="">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($referrals as $referral): ?>
            <tr>
                <td width=""><?php echo $referral->name; ?></td>
                <td width=""><?php echo $referral->id; ?></td>
                <td width=""><?php echo $referral->email; ?></td>
                <td width=""><?php echo $referral->completed > 0 ? 'Complete' : 'Pending'; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody> 
</table>
