<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="page-title">Refer a Friend</div>
<div class="refer-friends">
    <P>Get your friends to sign up using the following link :</P>
    <br />
    <div>
        <?php
        $data = array(
            'name' => 'signuplink',
            'id' => 'signuplink',
            'value' => site_url('join/index/' . $user_id),
            'maxlength' => '',
            'size' => '',
            'style' => '',
            'class' => 'float-left'
        );
        echo form_input($data);

        $data = array(
            'name' => 'button',
            'id' => 'button',
            'value' => 'true',
            'type' => 'button',
            'content' => 'Copy',
            'class' => 'float-left'
        );
        echo form_button($data);
        ?>
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style float-left"> 
            <a href="http://www.addthis.com/bookmark.php?v=250&pubid=ra-4f8cdcdc18b0218f" _cke_saved_href="http://www.addthis.com/bookmark.php?v=250&pubid=ra-4f8cdcdc18b0218f" class="addthis_button_compact share-link"
               addthis:url="<?php echo site_url('join/index/' . $user_id); ?>"
               addthis:title="Refer your friends"
               addthis:description="Refer your friends">Share</a> 
        </div> 
        <script type="text/javascript">
            var addthis_config = {"data_track_addressbar":true};
        </script>
        <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f8cdcdc18b0218f"></script>
        <!-- AddThis Button END -->
    </div>
    <p></p>
    <br /><br /><br />
    <p>Once you have completed your offer its time to start getting your referrals to qualify for your free gifts</p>
    <p>Tips on getting referrals</p>

    <ul>
        <li>&bull; &nbsp; Copy the above referral link and paste it into an email and send it to everyone in your address book</li>
        <li>&bull; &nbsp; Add your referral link to your signature on any forum you are a member of (If this is allowed)</li>
        <li>&bull; &nbsp; Add your referral link to personal websites or blogs you contribute to</li>
        <li>&bull; &nbsp; Add your referral link to your facebook or twitter page</li>
        <li>&bull; &nbsp; Send your referral to your email contact list, letting them know about ucanpick.com</li>
        <li>&bull; &nbsp; Put your referral link on some business cards from VistaPrint</li>
    </ul>

</div>