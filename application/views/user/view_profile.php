<?php if (is_object($user)): ?>
    <div class="my-account">
        <?php
        echo anchor('auth/change_password', 'Click here to change your password', array('title' => 'Change your password!'));
        ?>
        <br /><br />
        <?php
        echo anchor('user/edit_profile', 'Click here to edit your profile', array('title' => 'Edit your profile!'));
        ?>
        <br /><br />
        <div class="left-column">
            <dl>
                <dt>Name</dt>
                <dd><?php echo $user->name; ?></dd>
                <dt>Address</dt>
                <dd><?php echo $user->address; ?></dd>
                <dt>City</dt>
                <dd><?php echo $user->city; ?></dd>
                <dt>Postal code</dt>
                <dd><?php echo $user->postal_code; ?></dd>
            </dl>
        </div>
        <div class="right-column">
            <dl>
                <dt>Postal code</dt>
                <dd><?php echo $user->postal_code; ?></dd>
                <dt>County</dt>
                <dd><?php echo $user->county; ?></dd>
                <dt>Country</dt>
                <dd><?php echo $user->country; ?></dd>
            </dl>
        </div>
    </div>
<?php endif; ?>
