<div class="my-account">

    <?php if (is_object($user)): ?>
        <?php
        $attributes = array('class' => '', 'id' => '');
        echo form_open('user/edit_profile/', $attributes);
        ?>
        <div class="left-column">
            <p>
                <?php echo form_error('name'); ?>
                <br /><input id="name" type="text" name="name" maxlength="100" placeholder="Name" value="<?php echo $user->name; ?>"  />
            </p>
            <p>
                <?php echo form_error('address'); ?>
                <br /><input id="address" type="text" name="address" maxlength="100" placeholder="Address" value="<?php echo $user->address; ?>"  />
            </p>
            <p>
                <?php echo form_error('city'); ?>
                <br /><input id="city" type="text" name="city" maxlength="100" placeholder="City" value="<?php echo $user->city; ?>"  />
            </p>
        </div>
        <div class="right-column">
            <p>
                <?php echo form_error('postal_code'); ?>
                <br /><input id="postal_code" type="text" name="postal_code" maxlength="100" placeholder="Postal Code" value="<?php echo $user->postal_code; ?>"  />
            </p>
            <p>
                <?php echo form_error('county'); ?>
                <br /><input id="county" type="text" name="county" maxlength="100" placeholder="State" value="<?php echo $user->county; ?>"  />
            </p>
            <p>
                <?php echo form_error('country'); ?>
                <br /><input id="country" type="text" name="country" maxlength="100" placeholder="Country" value="<?php echo $user->country; ?>"  />
            </p>
            <p>
                <?php
                $data = array(
                    'name' => 'submit',
                    'id' => 'submit',
                    'value' => 'true',
                    'type' => 'submit',
                    'content' => 'Save',
                    'class' => 'submit-button'
                );

                echo form_button($data);
                ?>
            </p>
        </div>

        <?php echo form_close(); ?>
    <?php endif; ?>
</div>
