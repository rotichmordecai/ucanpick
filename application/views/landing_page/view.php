<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="c_hero-unit">
    <div class="left-column bg_white">
        <div style="font-size: 20px; padding: 10px; font-weight: bold; border-bottom: solid 1px lightblue">
            <?php echo $landing_page->promotional_text; ?>
        </div>
        <div style="text-align: center; padding: 20px;">
            <?php
            $image_properties = array(
                'src' => 'images/' . $landing_page->image,
                'alt' => $landing_page->promotional_text,
                'class' => '',
                'width' => '200',
                'height' => '200',
                'title' => $landing_page->promotional_text,
                'rel' => '',
            );
            echo img($image_properties);
            ?>
        </div>
        <div style="padding: 10px;"><?php echo $landing_page->promotional_detail; ?></div>
    </div>
    <div class="right-column bg_white">
        <div style="font-size: 20px; padding: 10px; font-weight: bold; border-bottom: solid 1px lightblue">
            <?php echo $landing_page->general_promotional_text; ?>
        </div>
        <div>
            <?php
            $this->load->view('landing_page/register_form');
            ?>
        </div>
    </div>
</div>
<div class="left-column bg_white">
    <?php echo $landing_page->how_it_works; ?>
</div>
<div class="right-column bg_white">
    <?php echo $landing_page->types_of_rewards; ?>
</div>