<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div style="padding: 10px; font-size: 16px;">
    Signup now!
</div>
<?php
if ($use_username) {

    $username = array(
        'name' => 'username',
        'id' => 'username',
        'value' => set_value('username'),
        'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
        'size' => 30,
        'placeholder' => 'Username',
        'class' => 'input-xlarge'
    );
}

$email = array(
    'name' => 'email',
    'id' => 'email',
    'value' => set_value('email'),
    'maxlength' => 80,
    'size' => 30,
    'placeholder' => 'Email',
    'class' => 'input-xlarge'
);

$password = array(
    'name' => 'password',
    'id' => 'password',
    'value' => set_value('password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
    'placeholder' => 'Password',
    'class' => 'input-xlarge'
);

$confirm_password = array(
    'name' => 'confirm_password',
    'id' => 'confirm_password',
    'value' => set_value('confirm_password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
    'placeholder' => 'Confirm Password',
    'class' => 'input-xlarge'
);

$name = array(
    'name' => 'name',
    'id' => 'name',
    'value' => set_value('name'),
    'maxlength' => 80,
    'size' => 30,
    'placeholder' => 'Name',
    'class' => 'input-xlarge'
);

$captcha = array(
    'name' => 'captcha',
    'id' => 'captcha',
    'maxlength' => 8
);
?>
<?php
$attributes = array('class' => 'register', 'id' => '');
echo form_open('auth/register', $attributes);
?>

<?php if ($use_username) { ?>
    <?php echo form_input($username); ?>
    <?php echo form_error($username['name']); ?>
<?php } ?>
<?php echo form_input($name); ?>
<?php echo form_error($name['name']); ?>
<?php echo form_input($email); ?>
<?php //echo form_error($email['name']); ?>
<?php echo form_password($password); ?>
<?php //echo form_error($password['name']); ?>
<?php echo form_password($confirm_password); ?>
<?php //echo form_error($confirm_password['name']); ?>


<div class="captcha">
    <?php
    echo "<span class='captcha-text'>" . $this->session->userdata('sentence') . " " . $this->session->userdata('num1') . " " . $this->session->userdata('operand') . " " . $this->session->userdata('num2') . " ? " . "</span>";
    echo "<input type='text' name='answer' id='answer' size='5' />";
    ?>
</div>
<p class="terms-and-conditions">I agree to the terms and Conditions</p>
<input type="checkbox" name="termsandconditions" value="1" style="width: 15px;">
<br />
<?php
$data = array(
    'name' => 'register',
    'id' => 'register',
    'value' => 'true',
    'type' => 'submit',
    'content' => 'Continue',
    'class' => 'submit-button register-button'
);

echo form_button($data);
?>
<?php echo form_close(); ?>

