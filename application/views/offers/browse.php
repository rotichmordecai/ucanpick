<div class="page-title">ALL OFFERS</div>
<p style="margin-left: 15px;">
    Complete One of the Offers Below
</p>
<p style="margin-left: 15px;">
    Please allow at least 48-72 hours for details of any offer to show up in your account
</p>
<?php if (is_object($gift)): ?>
    <div class="success">
        You have currently selected <span style="font-weight: bold;"><?php echo $gift->name; ?></span> as your gift. Click <?php echo anchor('gifts', 'here', array('title' => 'Select another gift!')); ?> to select another gift
    </div>
<?php endif; ?>
<div class="c_offer-bar">
    <?php if (is_array($offers)): ?>
        <?php foreach ($offers as $offer): ?>
                <div class="c_offer">
                <a href="<?php echo site_url('offers/click/' . $offer->id); ?>" class="click_offer" url="<?php echo addhttp($offer->url); ?>">
                    <div class="offer_image">
                        <?php
                        $image_attributes = image_attributes($offer->image);
                        echo img(array('src' => $offer->image, 'class' => 'photo_middle', 'width' => $image_attributes ? $image_attributes['width'] : null, 'height' => $image_attributes ? $image_attributes['height'] : null, 'title' => 'Offer Image'));
                        ?>
                    </div>
                    <?php echo anchor('offers/click/' . $offer->id, $offer->title, array('title' => 'Complete Offer!')); ?>
                    </a>
                </div>
            
        <?php endforeach; ?>
    <?php endif; ?>
</div>