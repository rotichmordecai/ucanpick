<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
$attributes = array('class' => '', 'id' => '');
echo form_open(current_url(), $attributes);
?>

<dl class="dl-horizontal">
    <dt>User</dt>
    <dd><?php echo $offer->name; ?></dd>
    <dt>Offer Title</dt>
    <dd><?php echo $offer->title; ?></dd>
    <dt>Offer Image</dt>
    <dd><img src="<?php echo $offer->image; ?>" /></dd>
    <dt>Offer Url</dt>
    <dd><?php echo $offer->url; ?></dd>
    <dt>Gift</dt>
    <dd><?php echo form_dropdown('gift_id', $gifts_dropdown, $gift->id); ?></dd>

    <dt>Status</dt>
    <dd>
        <?php
        $options = array(
            0 => 'Pending',
            1 => 'Completed',
        );
        echo form_dropdown('success', $options, $offer->success);
        echo form_hidden('user_id', $offer->user_id);
        echo form_hidden('offer_id', $offer->offer_id);
        ?>
    </dd>
    <dt></dt>
    <dd>
        <?php
        echo form_submit('submit', 'Save!');
        ?>
    </dd>
</dl>

<?php
echo form_close();
?>