<h2>
    <a href="<?php echo site_url(); ?>/admin"> YOUCANPICK </a>  &raquo; Offers</h2>

<p><?php echo (!empty($msg) ? $msg : ''); ?></p>

<p>You currently have <?php echo count($items); ?> offer(s) </p>

<p><hr /></p>

<?php if (!empty($items)): ?>

    <table cellspacing="5" cellpadding="7">

        <tr>
            <?php foreach ($item_fields as $field): ?>
                <td><?php echo $field; ?></td>
            <?php endforeach; ?>
            <td colspan="2">Action</td>
        </tr>

        <?php for ($c = 0; $c < count($items); $c++): ?>
            <tr>
                <td><?php echo $items[$c]['id']; ?></td>
                <td><?php echo $items[$c]['title']; ?></td>
                <td><?php echo $items[$c]['image']; ?></td>
                <td><?php echo $items[$c]['description']; ?></td>
                <td><?php echo $items[$c]['url']; ?></td>
                <td><?php echo $items[$c]['created']; ?></td>
                <td><?php echo $items[$c]['expire']; ?></td>
                <td><?php echo $items[$c]['active']; ?></td>
                <td><a href="<?php echo site_url() . '/offer/edit_offer/' . $items[$c]['id']; ?>">Edit</a></td>
                <td><a href="<?php echo site_url() . '/offer/delete_offer/' . $items[$c]['id']; ?>" onclick="return confirm('Are you sure you want to delete?')" >Delete</a></td>
            </tr>
        <?php endfor; ?>

    </table>

<?php endif; ?>
<p><hr /></p>

<a href="<?php echo site_url(); ?>/offer/add_offer">Add offer</a>
