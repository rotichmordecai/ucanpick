<?php
// Change the css classes to suit your needs    

$attributes = array('class' => '', 'id' => '');
echo form_open_multipart('admin/add_offer', $attributes);
?>

<p>
    <label for="title">Title <span class="required">*</span></label>
    <?php echo form_error('title'); ?>
    <br /><input id="title" type="text" name="title" maxlength="75" value="<?php echo set_value('title'); ?>"  />
</p>

<p>
    <label for="title">Image <span class="required">*</span></label>
    <?php echo form_error('image'); ?>
    <br /><input id="image" type="text" name="image" maxlength="255" value="<?php echo set_value('image'); ?>"  />
</p>

<p>
    <label for="description">Description <span class="required">*</span></label>
    <?php echo form_error('description'); ?>
    <br />

    <?php echo form_textarea(array('name' => 'description', 'rows' => '5', 'cols' => '80', 'value' => set_value('description'))) ?>
</p>

<p>
    <label for="title">Url <span class="required">*</span></label>
    <?php echo form_error('url'); ?>
    <br /><input id="url" type="text" name="url" maxlength="255" value="<?php echo set_value('url'); ?>"  />
</p>

<p>
    <label for="active">Active <span class="required">*</span></label>
    <?php echo form_error('active'); ?>

    <?php // Change the values in this array to populate your dropdown as required ?>
    <?php
    $options = array(
        '' => 'Please Select',
        '1' => 'Yes',
        '0' => 'No'
    );
    ?>

    <br /><?php echo form_dropdown('active', $options, set_value('active')) ?>
</p> 

<p>
    <?php echo form_submit('submit', 'Submit'); ?>
</p>

<?php echo form_close(); ?>
<hr />