<?php if (is_object($gift)): ?>

    <?php
// Change the css classes to suit your needs    

    $attributes = array('class' => '', 'id' => '');
    echo form_open_multipart('admin/edit_gift/' . $gift->id, $attributes);
    ?>

    <p>
        <label for="title">Name <span class="required">*</span></label>
        <?php echo form_error('name'); ?>
        <br /><input id="name" type="text" name="name" maxlength="100" value="<?php echo $gift->name; ?>"  />
    </p>

    <p>
        <label for="title">Image <span class="required">*</span></label>
        <?php echo form_error('image'); ?>
        <br />
        <?php
        $data = array(
            'name' => 'image',
            'id' => 'image',
            'value' => '',
            'maxlength' => '',
            'size' => '',
            'style' => '',
            'type' => 'file'
        );
        echo form_input($data);
        ?>
    </p>

    <p>
        <label for="description">Description <span class="required">*</span></label>
        <?php echo form_error('description'); ?>
        <br />

        <?php echo form_textarea(array('name' => 'description', 'rows' => '5', 'cols' => '80', 'value' => $gift->description)) ?>
    </p>

    <p>
        <label for="title">Referrals needed <span class="required">*</span></label>
        <?php echo form_error('referrals'); ?>
        <br /><input id="referrals" type="text" name="referrals" maxlength="45" value="<?php echo $gift->referrals; ?>"  />
    </p>

    <p>
        <label for="active">Active <span class="required">*</span></label>
        <?php echo form_error('active'); ?>

        <br /><?php echo form_dropdown('active', $active_options, $gift->active) ?>
    </p>                                             


    <p>
        <?php echo form_submit('submit', 'Submit'); ?>
    </p>

    <?php echo form_close(); ?>

<?php endif; ?>
