<div class="span3">
    <div class="well sidebar-nav">
        <ul class="nav nav-list">
            <li class="nav-header">Sidebar</li>
            <li class="active"><a href="#">Dashboard</a></li>
            <li><?php echo anchor('admin/manage_offers', 'Offers', array('title' => 'Manage Offers!')); ?></li>
            <li><?php echo anchor('admin/manage_gifts', 'Gifts', array('title' => 'Manage Gifts!')); ?></li>
            <li><?php echo anchor('admin/manage_users', 'Users', array('title' => 'Manage Users!')); ?></li>
            <li><?php echo anchor('admin/manage_pages', 'Pages', array('title' => 'Manage Pages!')); ?></li>
            <li><?php echo anchor('admin/manage_landing_pages', 'Landing Pages', array('title' => 'Manage Landing Pages!')); ?></li>
        </ul>
    </div><!--/.well -->
</div><!--/span-->
