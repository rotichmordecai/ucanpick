<?php
$attributes = array('class' => '', 'id' => '');
echo form_open_multipart('admin/add_gift', $attributes);
?>

<p>
    <label for="title">Name <span class="required">*</span></label>
    <?php echo form_error('name'); ?>
    <br /><input id="title" type="text" name="name" maxlength="45" value="<?php echo set_value('name'); ?>"  />
</p>
<p>
    <label for="title">Image <span class="required">*</span></label>
    <?php echo form_error('image'); ?>
    <br />
    <?php
    $data = array(
        'name' => 'image',
        'id' => 'image',
        'value' => '',
        'maxlength' => '',
        'size' => '',
        'style' => '',
        'type' => 'file'
    );
    echo form_input($data);
    ?>
</p>

<p>
    <label for="description">Description <span class="required">*</span></label>
    <?php echo form_error('description'); ?>
    <br />

    <?php echo form_textarea(array('name' => 'description', 'rows' => '5', 'cols' => '80', 'value' => set_value('description'))) ?>
</p>

<p>
    <label for="title">Referrals needed <span class="required">*</span></label>
    <?php echo form_error('referrals'); ?>
    <br /><input id="referrals" type="text" name="referrals" maxlength="45" value="<?php echo set_value('referrals'); ?>"  />
</p>

<p>
    <label for="active">Active <span class="required">*</span></label>
    <?php echo form_error('active'); ?>
    <?php // Change the values in this array to populate your dropdown as required ?>
    <?php
    $options = array(
        '' => 'Please Select',
        '1' => 'Yes',
        '0' => 'No'
    );
    ?>

    <br /><?php echo form_dropdown('active', $options, set_value('active')) ?>
</p> 

<p>
    <?php echo form_submit('submit', 'Submit'); ?>
</p>

<?php echo form_close(); ?>
<hr />