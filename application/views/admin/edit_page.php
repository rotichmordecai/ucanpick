<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if (is_object($page)): ?>
    <?php
// Change the css classes to suit your needs    

    $attributes = array('class' => '', 'id' => '');
    echo form_open_multipart('admin/edit_page/' . $page->slug, $attributes);
    ?>

    <p>
        <label for="title">Title <span class="required">*</span></label>
        <?php echo form_error('title'); ?>
        <br /><input id="title" type="text" name="title" maxlength="100" value="<?php echo $page->title; ?>"  />
    </p>

    <p>
        <label for="description">Description <span class="required">*</span></label>
        <?php echo form_error('description'); ?>
        <br />

        <?php echo form_textarea(array('name' => 'description', 'rows' => '7', 'class' => 'tinymce', 'cols' => '200', 'value' => $page->description)) ?>
    </p>                                       


    <p>
        <?php echo form_submit('submit', 'Submit'); ?>
    </p>

    <?php echo form_close(); ?>
<?php endif; ?>
