<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if (is_object($user)): ?>
    <?php
    // Change the css classes to suit your needs    

    $attributes = array('class' => '', 'id' => '');
    echo form_open(current_url(), $attributes);
    ?>

    <p>
        <label for="title">Name <span class="required">*</span></label>
        <?php echo form_error('name'); ?>
        <br /><input id="name" type="text" name="name" maxlength="100" value="<?php echo $user->name; ?>"  />
    </p>

    <p>
        <label for="title">Address <span class="required">*</span></label>
        <?php echo form_error('address'); ?>
        <br /><input id="address" type="text" name="address" maxlength="100" value="<?php echo $user->address; ?>"  />
    </p>

    <p>
        <label for="title">City <span class="required">*</span></label>
        <?php echo form_error('city'); ?>
        <br /><input id="city" type="text" name="city" maxlength="100" value="<?php echo $user->city; ?>"  />
    </p>

    <p>
        <label for="title">Postal Code <span class="required">*</span></label>
        <?php echo form_error('postal_code'); ?>
        <br /><input id="postal_code" type="text" name="postal_code" maxlength="100" value="<?php echo $user->postal_code; ?>"  />
    </p>

    <p>
        <label for="title">County <span class="required">*</span></label>
        <?php echo form_error('county'); ?>
        <br /><input id="county" type="text" name="county" maxlength="100" value="<?php echo $user->county; ?>"  />
    </p>

    <p>
        <label for="title">Country <span class="required">*</span></label>
        <?php echo form_error('country'); ?>
        <br /><input id="country" type="text" name="country" maxlength="100" value="<?php echo $user->country; ?>"  />
    </p>

    <p>
        <?php echo form_submit('submit', 'Submit'); ?>
    </p>

    <?php echo form_close(); ?>

<?php endif; ?>
