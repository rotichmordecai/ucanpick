<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
$attributes = array('class' => '', 'id' => '');
echo form_open_multipart(current_url(), $attributes);
?>

<p>
    <label for="general_promotional_text">General Promotional Text <span class="required">*</span></label>
    <?php echo form_error('general_promotional_text'); ?>
    <br /><input id="general_promotional_text" type="text" name="general_promotional_text" maxlength="45" value="<?php echo set_value('general_promotional_text', $landing_page->general_promotional_text); ?>"  />
</p>

<p>
    <label for="promotional_text">Promotional Text <span class="required">*</span></label>
    <?php echo form_error('promotional_text'); ?>
    <br /><input id="promotional_text" type="text" name="promotional_text" maxlength="45" value="<?php echo set_value('promotional_text', $landing_page->promotional_text); ?>"  />
</p>

<p>
    <label for="promotional_detail">Promotional Detail <span class="required">*</span></label>
    <?php echo form_error('promotional_detail'); ?>
    <br />

    <?php echo form_textarea(array('name' => 'promotional_detail', 'rows' => '5', 'cols' => '80', 'value' => set_value('promotional_detail', $landing_page->promotional_detail))) ?>
</p>

<p>
    <label for="title">Image <span class="required">*</span></label>
    <?php echo form_error('image'); ?>
    <br />
    <?php
    $data = array(
        'name' => 'image',
        'id' => 'image',
        'value' => '',
        'maxlength' => '',
        'size' => '',
        'style' => '',
        'type' => 'file'
    );
    echo form_input($data);
    ?>
</p>

<p>
    <label for="how_it_works">How it works <span class="required">*</span></label>
    <?php echo form_error('how_it_works'); ?>
    <br />
    <?php echo form_textarea(array('name' => 'how_it_works', 'rows' => '5', 'cols' => '80', 'value' => set_value('how_it_works',$landing_page->how_it_works))) ?>
</p>

<p>
    <label for="types_of_rewards">Types of rewards <span class="required">*</span></label>
    <?php echo form_error('types_of_rewards'); ?>
    <br />
    <?php echo form_textarea(array('name' => 'types_of_rewards', 'rows' => '5', 'cols' => '80', 'value' => set_value('types_of_rewards',$landing_page->types_of_rewards))) ?>
</p>

<p>
    <label for="active">Active <span class="required">*</span></label>
    <?php echo form_error('active'); ?>
    <?php // Change the values in this array to populate your dropdown as required ?>
    <?php
    $options = array(
        '' => 'Please Select',
        '1' => 'Yes',
        '0' => 'No'
    );
    ?>

    <br /><?php echo form_dropdown('active', $options, set_value('active',$landing_page->active)) ?>
</p> 

<p>
    <?php echo form_submit('submit', 'Submit'); ?>
</p>

<?php echo form_close(); ?>
<hr />
