<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="user_offers" >
    <thead>
        <tr>
            <th width="">Id</th>
            <th width="">Title</th>
            <th width="">Image</th>
            <th width="">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($offers as $offer): ?>
            <tr>
                <td width=""><?php echo $offer->id; ?></td>
                <td width=""><?php echo $offer->title; ?></td>
                <td width=""><img src="<?php echo $offer->image; ?>" /></td>
                <td width=""><?php echo anchor('admin/edit_user_offer/' . $offer->user_id . '/' . $offer->offer_id, 'Status', array('title' => 'Status!')); ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody> 
</table>
