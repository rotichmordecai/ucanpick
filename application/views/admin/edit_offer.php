<?php if (is_object($offer)): ?>
    <?php
    // Change the css classes to suit your needs    
    $attributes = array('class' => '', 'id' => '');
    echo form_open_multipart('admin/edit_offer/' . $offer->id, $attributes);
    ?>

    <p>
        <label for="title">Title <span class="required">*</span></label>
        <?php echo form_error('title'); ?>
        <br /><input id="title" type="text" name="title" maxlength="75" value="<?php echo $offer->title; ?>"  />
    </p>

    <p>
        <label for="title">Image <span class="required">*</span></label>
        <?php echo form_error('image'); ?>
        <br /><input id="image" type="text" name="image" maxlength="255" value="<?php echo $offer->image; ?>"  />
    </p>

    <p>
        <label for="description">Description <span class="required">*</span></label>
        <?php echo form_error('description'); ?>
        <br />
        <?php echo form_textarea(array('name' => 'description', 'rows' => '5', 'cols' => '80', 'value' => $offer->description)) ?>
    </p>

    <p>
        <label for="title">Url <span class="required">*</span></label>
        <?php echo form_error('url'); ?>
        <br /><input id="url" type="text" name="url" maxlength="255" value="<?php echo $offer->url; ?>"  />
    </p>

    <p>
        <label for="active">Active <span class="required">*</span></label>
        <?php echo form_error('active'); ?>

        <br /><?php echo form_dropdown('active', $active_options, $offer->active) ?>
    </p>                                             


    <p>
        <?php echo form_submit('submit', 'Submit'); ?>
    </p>

    <?php echo form_close(); ?>

<?php endif; ?>
