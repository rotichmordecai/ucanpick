<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if (is_object($page)): ?>
    <div class="c_hero-unit pages">
        <div class="page-title">
            <?php echo $page->title; ?>
        </div>
        <div style="text-align: justify; padding: 15px;">
            <?php echo $page->description; ?>
        </div>
        <div>
            <?php
            if (!$this->tank_auth->is_logged_in()) {
                echo anchor('auth/register', 'Join Now !', array('title' => 'Join Now!','class' => 'join-now'));
            }
            ?>
        </div>
    </div>
<?php endif; ?>

