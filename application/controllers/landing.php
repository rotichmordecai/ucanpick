<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing extends MY_Controller {

    protected $template_path;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('landing_page_model');

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        parent::load_landing_page();

        $this->template_path = 'landing_page/';
    }

    function view($page_id) {
        $data = array();
        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
        }
        $data['use_username'] = $use_username;
        $data['landing_page'] = $this->landing_page_model->view_landing_page($page_id);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

}

?>