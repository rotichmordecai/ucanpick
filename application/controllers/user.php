<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MY_Controller {

    protected $template_path;

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $this->load->model('offer_model');
        $this->load->model('gift_model');

        $this->template_path = 'user/';
        parent::load_single_column();

        if (!$this->tank_auth->is_logged_in()) { // not logged on
            redirect('auth/login');
        }
    }

    function index() {
        $this->view_profile();
    }

    function edit_profile() {

        $data = array();
        $data['title'] = 'My Account';
        $user_id = $this->tank_auth->get_user_id();
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        $this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
        $this->form_validation->set_rules('city', 'City', 'required|max_length[255]');
        $this->form_validation->set_rules('postal_code', 'Postal code', 'required|max_length[255]');
        $this->form_validation->set_rules('county', 'County', 'required|max_length[255]');
        $this->form_validation->set_rules('country', 'Country', 'required|max_length[255]');

        //get user_profile
        $data['user'] = $this->tank_auth->get_user_profile($user_id);

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            // build array for the model
            $form_data = array(
                'name' => set_value('name'),
                'address' => set_value('address'),
                'city' => set_value('city'),
                'postal_code' => set_value('postal_code'),
                'county' => set_value('county'),
                'country' => set_value('country')
            );

            // run insert model to write data to db
            if ($this->tank_auth->update_user_profile($user_id, $form_data) == TRUE) { // the information has therefore been successfully saved in the db
                redirect('user/view_profile');
            } else {
                redirect('user/view_profile');
            }
        }
    }

    function view_profile() {

        $data = array();
        $data['title'] = 'My Account';
        $user_id = $this->tank_auth->get_user_id();
        $data['user'] = $this->tank_auth->get_user_profile($user_id);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function refer() {

        $data = array();
        $data['user_id'] = $this->tank_auth->get_user_id();
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function status() {
        $data = array();
        $data['title'] = 'My Status';
        $user_id = $this->tank_auth->get_user_id();
        $data['user'] = $this->tank_auth->get_user_profile($user_id);
        $data['gift'] = $this->users->user_gift($user_id);
        $data['offers'] = $this->users->user_offers($user_id);
        $data['referrals'] = $this->users->user_referrals($user_id);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

}

?>