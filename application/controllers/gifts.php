<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gifts extends MY_Controller {

    protected $template_path;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('gift_model');

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        parent::load_single_column();

        $this->template_path = 'gifts/';

        if (!$this->tank_auth->is_logged_in()) { // not logged on
            redirect('auth/login');
        }
    }

    function index() {
        return $this->browse();
    }

    function browse() {

        $data = array();
        $user_id = $this->tank_auth->get_user_id();
        if ($this->input->post('suggestgift') != null) {

            $this->load->library('email');

            $this->email->from('noreply@ucanpick.com', 'NoReply');
            $this->email->to('korir.mordecai@gmail.com');
            $this->email->cc('joeanto@gmail.com');
            $this->email->subject('#' . $user_id . ' gift suggestion [www.ucanpick.com]');
            $this->email->message('User ' . $user_id . ' has suggested : ' . $this->input->post('suggestgift'));
            $this->email->send();
            
            $this->session->set_flashdata('message', 'Thank you, We will get back to you shortly');
            
        }
        $data['gift'] = null;
        if ($this->session->userdata('selected_gift_id') != null) {
            $data['gift'] = $this->gift_model->get_gift($this->session->userdata('selected_gift_id'));
        }
        $data['gifts'] = $this->gift_model->view_gifts();
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function view($gift_id) {
        $data = array();
        $data['gift'] = $this->gift_model->get_gift($gift_id);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function click($gift_id) {
        $user_id = $this->tank_auth->get_user_id();
        $this->session->set_userdata('selected_gift_id', $gift_id);
        $this->gift_model->suggested_gift($user_id, $gift_id);
        redirect('offers');
    }

}

?>