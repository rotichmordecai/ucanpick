<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of page
 *
 * @author rotich
 */
class Page extends MY_Controller {

    protected $template_path;

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $this->load->model('page_model');

        $this->template_path = 'page/';
        parent::load_single_column();
    }

    function view($slug) {
        $data = array();
        $data['page'] = $this->page_model->view($slug);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

}

?>
