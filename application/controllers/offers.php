<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Offers extends MY_Controller {

    protected $template_path;

    function __construct() {

        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('offer_model');
        $this->load->model('gift_model');

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        parent::load_single_column();

        $this->template_path = 'offers/';

        if (!$this->tank_auth->is_logged_in()) { // not logged on
            redirect('auth/login');
        }
    }

    function index() {
        return $this->browse();
    }

    function browse() {

        $data = array();
        $data['gift'] = null;
        if ($this->session->userdata('selected_gift_id') != null) {
            $data['gift'] = $this->gift_model->get_gift($this->session->userdata('selected_gift_id'));
        }
        $data['offers'] = $this->offer_model->view_offers();
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function view($offer_id) {

        $data = array();
        $data['offer'] = $this->offer_model->get_offer($offer_id);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function click($offer_id) {

        $user_id = $this->tank_auth->get_user_id();
        $offer = $this->offer_model->get_offer($offer_id);

        if (is_object($offer)) {
            $this->offer_model->save_click($user_id, $offer_id);
            //header('Location:' . addhttp($offer->url), TRUE, 301);
            //echo '<script type="text/javascript" language="Javascript">window.open(\'' . addhttp($offer->url) . '\');</script>';
            exit(0);
        }
    }

}

?>