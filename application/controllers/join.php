<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of join
 *
 * @author rotich
 */
class Join extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
    }

    function index($referrer = null) {
        if (isset($referrer)) {
            $this->session->set_userdata('referrer', $referrer);
        }
        redirect('auth/register');
    }

}

?>
