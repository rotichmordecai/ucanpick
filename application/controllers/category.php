<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('category_model');
    }

    function index() {
        return $this->view_categories();
    }

    function view_categories($msg = '') {
        $data['items'] = $this->category_model->GetCategories();
        $data['item_fields'] = $this->category_model->GetCategoryFields();
        $data['msg'] = $msg;
        $this->load->view('category/view_category_view', $data);
    }

    function add_category() {
        $this->form_validation->set_rules('id', 'ID', 'required|trim|max_length[11]');
        $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[45]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->load->view('category/add_category_view');
        } else { // passed validation proceed to post success logic
            // build array for the model

            $form_data = array(
                'id' => set_value('id'),
                'active' => set_value('active'),
                'title' => set_value('title'),
                'description' => set_value('description')
            );

            // run insert model to write data to db

            if ($this->category_model->SaveCategory($form_data) == TRUE) {
                $msg = 'Category added successfully!';
                return $this->view_categories($msg);
            } else {
                $msg = 'An error occurred saving your information. Please try again later';
                return $this->view_categories($msg);
            }
        }
    }

    function success() {
        $msg = 'this form has been successfully submitted with all validation being passed. All messages or logic here. Please note
			sessions have not been used and would need to be added in to suit your app';

        return $this->view_categories($msg);
    }

    function edit_category($id) {
        $this->form_validation->set_rules('id', 'ID', 'required|trim|max_length[11]');
        $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[45]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        //get category
        $data['item'] = $item = $this->category_model->GetCategory($id);

        $data['active_options'] = $this->category_model->GetCategoryStatuses();

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->load->view('category/edit_category_view', $data);
        } else { // passed validation proceed to post success logic
            // build array for the model

            $form_data = array(
                'id' => set_value('id'),
                'title' => set_value('title'),
                'description' => set_value('description'),
                'active' => set_value('active')
            );

            // run insert model to write data to db

            if ($this->category_model->UpdateCategory($form_data, $item['id']) == TRUE) { // the information has therefore been successfully saved in the db
                $msg = "Update successful!";
                return $this->view_categories($msg);
            } else {
                $msg = 'An error occurred updating your information. Please try again later';
                return $this->view_categories($msg);
            }
        }
    }

    function delete_category($id) {
        if ($this->category_model->DeleteCategory($id) == TRUE) { // the information has therefore been successfully deleted from the db
            $msg = 'Category deleted successfully!';
            return $this->view_categories($msg);
        } else {
            $msg = 'Error encountered! The category has not been deleted';
            return $this->view_categories($msg);
        }
    }

}

?>