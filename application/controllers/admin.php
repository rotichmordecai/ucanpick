<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {

    protected $template_path;

    function __construct() {

        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $this->load->model('gift_model');
        $this->load->model('offer_model');
        $this->load->model('page_model');
        $this->load->model('landing_page_model');
        $this->load->model('tank_auth/users');

        $this->template_path = 'admin/';

        $this->load->library('datatables');

        $this->template->set_template('admin_single_column');

        $this->template->add_css('assets/css/bootstrap.css');
        $this->template->add_css('assets/css/dataTables.bootstrap.css');
        $this->template->add_css('assets/css/jquery.lightbox-0.5.css');
        //$this->template->add_css('assets/css/bootstrap-responsive.css');
        $this->template->add_css('body { padding-top: 100px; padding-bottom: 40px; }', 'embed', 'print');
        $this->template->add_js('assets/js/jquery-1.8.1.min.js');
        $this->template->add_js('assets/js/bootstrap.min.js');
        $this->template->add_js('assets/js/jquery.dataTables.min.js');
        $this->template->add_js('assets/js/dataTables.bootstrap.js');
        $this->template->add_js('assets/js/tiny_mce/jquery.tinymce.js');
        $this->template->add_js('assets/js/jquery.lightbox-0.5.js');
        $this->template->add_js('assets/js/jquery.placeholder.js');
        $this->template->add_js('assets/js/application.js');

        if (!$this->tank_auth->is_logged_in()) { // not logged on
            redirect('auth/login');
        }

        if ($this->session->userdata('is_admin') != 1) {
            redirect('auth/logout');
        }

        $this->output->enable_profiler(FALSE);
    }

    function index() {

        $data = array();
        $data['title'] = 'Administrator';
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function manage_offers() {
        $data = array();
        $data['title'] = 'Manage Offers';
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function manage_gifts() {
        $data = array();
        $data['title'] = 'Manage Gifts';
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function manage_users() {
        $data = array();
        $data['title'] = 'Manage Users';
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function manage_pages() {
        $data = array();
        $data['title'] = 'Manage Pages';
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function manage_landing_pages() {
        $data = array();
        $data['title'] = 'Manage Landing Pages';
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function user_referrals($user_id) {
        $data = array();
        $data['title'] = 'User Referrals';
        $data['referrals'] = $this->users->user_referrals($user_id);
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function referrer_gift($referrer, $user_id) {

        $data = array();
        $data['title'] = 'Award Gift to Referrer';
        $gifts = $this->gift_model->view_gifts();
        $gifts_dropdown = array();

        foreach ($gifts as $gift) {
            $gifts_dropdown[$gift->id] = $gift->name;
        }

        if ($this->input->post('gift_id') != '') {
            $insert_array = array(
                'referrer' => $referrer,
                'user_id' => $user_id,
                'gift_id' => $this->input->post('gift_id'),
                'status' => 1
            );

            if ($this->users->save_referrer_gift($insert_array)) {
                redirect('admin/user_referrals/' . $referrer);
            }
        }

        $data['gifts_dropdown'] = $gifts_dropdown;
        $data['user_referrer'] = $this->users->user_referrer($referrer, $user_id);
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function user_offers($user_id) {
        $data = array();
        $data['title'] = 'User Offers';
        $data['offers'] = $this->users->user_offers($user_id);
        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function edit_user_offer($user_id, $offer_id) {

        $data = array();
        $data['title'] = 'Edit Offer Status';

        $gifts = $this->gift_model->view_gifts();
        $gifts_dropdown = array();
        foreach ($gifts as $gift) {
            $gifts_dropdown[$gift->id] = $gift->name;
        }
        $data['gifts_dropdown'] = $gifts_dropdown;

        if ($this->input->post('success') != null) {

            $update_array = array(
                'gift_id' => $this->input->post('gift_id'),
                'success' => $this->input->post('success')
            );

            $this->users->edit_user_offer($user_id, $offer_id, $update_array);
        }

        $data['gift'] = $this->users->user_gift($user_id);
        $data['offer'] = $this->users->get_user_offer($user_id, $offer_id);

        $this->template->write_view('sidebar', 'admin/sidebar', $data);
        $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
        $this->template->render();
    }

    function json_gifts() {

        $this->datatables
                ->select('id, name, image, referrals')
                ->from('gift')
                ->add_column('image', img(array('src' => 'images/$1', 'alt' => 'Image', 'class' => 'img-rounded', 'width' => '', 'height' => '', 'title' => 'Gift Image', 'rel' => '')), 'image')
                ->add_column('edit', anchor('admin/edit_gift/$1', 'Edit', array('title' => 'The best news!')), 'id')
                ->add_column('delete', anchor('admin/delete_gift/$1', 'Delete', array('title' => 'The best news!')), 'id');

        echo $this->datatables->generate();
    }

    function json_pages() {

        $this->datatables
                ->select('id, title, slug')
                ->from('page')
                ->add_column('edit', anchor('admin/edit_page/$1', 'Edit', array('title' => 'Edit Page!')), 'slug');
        echo $this->datatables->generate();
    }

    function json_landing_pages() {

        $this->datatables
                ->select('id, promotional_text')
                ->from('landing_page')
                ->add_column('edit', anchor('admin/edit_landing_page/$1', 'Edit', array('title' => 'Edit Page!')), 'id')
                ->add_column('delete', anchor('admin/delete_landing_page/$1', 'Delete', array('title' => 'Delete Page!')), 'id');
        echo $this->datatables->generate();
    }

    function json_offers() {

        $this->datatables
                ->select('id, title, image')
                ->from('offer')
                ->add_column('image', image('$1'), 'image')
                ->add_column('edit', anchor('admin/edit_offer/$1', 'Edit', array('title' => 'The best news!')), 'id')
                ->add_column('delete', anchor('admin/delete_offer/$1', 'Delete', array('title' => 'The best news!')), 'id');

        echo $this->datatables->generate();
    }

    function json_users() {

        $this->datatables
                ->select('user_profile.user_id, user_profile.name, user.email ')
                ->select('(SELECT COUNT(*) from user_has_offer WHERE user.id= user_id AND success = 1) as completed_offers')
                ->select('(SELECT COUNT(*) from user_has_offer WHERE user.id= user_id) as all_offers')
                ->select('(SELECT COUNT(*) FROM user_profile where user_profile.referrer = user.id and (SELECT COUNT(*) FROM user_has_offer WHERE user_has_offer.success = 1) > 0) as completed_referrals')
                ->select('(SELECT COUNT(*) FROM user_profile where user_profile.referrer = user.id) as all_referrals')
                ->from('user')
                ->join('user_profile', 'user_profile.user_id = user.id')
                ->join('user_profile as user_profile2', 'user_profile2.user_id = user_profile.referrer', 'left')
                ->select('user_profile2.name as referrer')
                ->join('user_has_gift', 'user_has_gift.user_id = user.id', 'left')
                ->join('gift', 'gift.id = user_has_gift.gift_id', 'left')
                ->select('gift.name as gift_name')
                ->select('gift.referrals as referrals_needed')
                ->add_column('referrals', anchor('admin/user_referrals/$1', 'Referrals[$2/$3]', array('title' => 'User Referrals!')), 'user_profile.user_id, completed_referrals, all_referrals')
                ->add_column('offers', anchor('admin/user_offers/$1', 'Offers [$2/$3]', array('title' => 'User Offers!')), 'user_profile.user_id, completed_offers, all_offers')
                ->add_column('edit', anchor('admin/edit_user/$1', 'Edit', array('title' => 'Edit user!')), 'user_profile.user_id')
                ->add_column('ban', anchor('admin/ban_user/$1', 'Ban', array('title' => 'Ban user!')), 'user_profile.user_id');

        echo $this->datatables->generate();
    }

    function add_gift() {

        $data = array();
        $data['title'] = 'Add Gift';
        $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[45]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('referrals', 'Referrals needed', 'required');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else {

            $upload_error = "";
            $image = 'placeholder.jpg';

            $this->imaging->upload($_FILES["image"]);

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = smart_url(set_value('name'));
                $this->imaging->process('./images/');

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $upload_error .= $this->imaging->error;
                }
            } else {
                $upload_error .= $this->imaging->error;
            }

            // build array for the model
            $form_data = array(
                'name' => set_value('name'),
                'image' => $image,
                'referrals' => set_value('referrals'),
                'active' => set_value('active'),
                'description' => set_value('description')
            );

            if ($this->gift_model->save_gift($form_data) == TRUE) {
                redirect('admin/manage_gifts');
            } else {
                redirect('admin/manage_gifts');
            }
        }
    }

    function ban_user($user_id) {

        $this->users->ban_user($user_id);
        redirect('admin/manage_users');
    }

    function edit_user($user_id) {

        $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        $this->form_validation->set_rules('address', 'Address', 'required|max_length[255]');
        $this->form_validation->set_rules('city', 'City', 'required|max_length[255]');
        $this->form_validation->set_rules('postal_code', 'Postal code', 'required|max_length[255]');
        $this->form_validation->set_rules('county', 'County', 'required|max_length[255]');
        $this->form_validation->set_rules('country', 'Country', 'required|max_length[255]');

        //get user_profile
        $data['user'] = $this->tank_auth->get_user_profile($user_id);

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            // build array for the model
            $form_data = array(
                'name' => set_value('name'),
                'address' => set_value('address'),
                'city' => set_value('city'),
                'postal_code' => set_value('postal_code'),
                'county' => set_value('county'),
                'country' => set_value('country')
            );

            // run insert model to write data to db
            if ($this->tank_auth->update_user_profile($user_id, $form_data) == TRUE) { // the information has therefore been successfully saved in the db
                redirect('admin/manage_users');
            } else {
                redirect('admin/manage_users');
            }
        }
    }

    function edit_page($slug) {

        $data = array();
        $data['title'] = 'Edit Page';
        $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[70]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[20000]');

        //get gift
        $data['page'] = $gift = $this->page_model->view($slug);

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $form_data = array(
                'title' => set_value('title'),
                'description' => $this->input->post('description')
            );
            // run insert model to write data to db

            if ($this->page_model->edit_page($form_data, $slug) == TRUE) { // the information has therefore been successfully saved in the db
                redirect('admin/manage_pages');
            } else {
                redirect('admin/manage_pages');
            }
        }
    }

    function edit_gift($id) {

        $data = array();
        $data['title'] = 'Edit Gift';
        $this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[45]');
        $this->form_validation->set_rules('image', 'Image', '');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('referrals', 'Referrals needed', 'required');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        //get gift
        $data['gift'] = $gift = $this->gift_model->get_gift($id);

        $data['active_options'] = array(
            '' => 'Please Select',
            '1' => 'Yes',
            '0' => 'No'
        );

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $upload_error = "";

            $image = $gift->image;

            $this->imaging->upload($_FILES["image"]);

            if ($this->imaging->uploaded == true) {

                if (is_file("./images/" . $image) && $image != 'placeholder.jpg') {
                    unlink("./images/" . $image);
                }

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = smart_url(set_value('name'));
                $this->imaging->process('./images/');

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $upload_error .= $this->imaging->error;
                }
            } else {
                $upload_error .= $this->imaging->error;
            }

            $form_data = array(
                'name' => set_value('name'),
                'image' => $image,
                'referrals' => set_value('referrals'),
                'active' => set_value('active'),
                'description' => set_value('description')
            );
            // run insert model to write data to db

            if ($this->gift_model->update_gift($form_data, $id) == TRUE) { // the information has therefore been successfully saved in the db
                redirect('admin/manage_gifts');
            } else {
                redirect('admin/manage_gifts');
            }
        }
    }

    function delete_gift($gift_id) {

        if ($this->gift_model->delete_gift($gift_id) == TRUE) { // the information has therefore been successfully deleted from the db
            redirect('admin/manage_gifts');
        } else {
            redirect('admin/manage_gifts');
        }
    }

    function add_offer() {

        $data = array();
        $data['title'] = 'Add Offer';
        $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[75]');
        $this->form_validation->set_rules('image', 'Image', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('url', 'Url', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $form_data = array(
                'category_id' => 1,
                'title' => set_value('title'),
                'image' => set_value('image'),
                'description' => set_value('description'),
                'url' => set_value('url'),
                'active' => set_value('active')
            );

            // run insert model to write data to db

            if ($this->offer_model->save_offer($form_data) == TRUE) {
                redirect('admin/manage_offers');
            } else {
                redirect('admin/manage_offers');
            }
        }
    }

    function add_landing_page() {

        $data = array();
        $data['title'] = 'New Landing Page';
        $this->form_validation->set_rules('general_promotional_text', 'General Promotional Text', 'required|trim|max_length[75]');
        $this->form_validation->set_rules('promotional_text', 'Promotional Text', 'required|trim|max_length[75]');
        $this->form_validation->set_rules('promotional_detail', 'Promotional Details', 'required|max_length[255]');
        $this->form_validation->set_rules('how_it_works', 'How it works', 'required|max_length[500]');
        $this->form_validation->set_rules('types_of_rewards', 'Types of Rewards', 'required|max_length[500]');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else {

            $upload_error = "";
            $image = 'placeholder.jpg';

            $this->imaging->upload($_FILES["image"]);

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = smart_url(set_value('promotional_text'));
                $this->imaging->process('./images/');

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $upload_error .= $this->imaging->error;
                }
            } else {
                $upload_error .= $this->imaging->error;
            }

            // build array for the model
            $form_data = array(
                'general_promotional_text' => set_value('general_promotional_text'),
                'promotional_text' => set_value('promotional_text'),
                'promotional_detail' => set_value('promotional_detail'),
                'image' => $image,
                'how_it_works' => set_value('how_it_works'),
                'types_of_rewards' => set_value('types_of_rewards'),
                'active' => set_value('active')
            );

            if ($this->landing_page_model->save_landing_page($form_data) == TRUE) {
                redirect('admin/manage_landing_pages');
            } else {
                redirect('admin/manage_landing_pages');
            }
        }
    }

    function edit_landing_page($page_id) {

        $data = array();
        $landing_page = $this->landing_page_model->view_landing_page($page_id);
        $data['landing_page'] = $landing_page;
        $data['title'] = 'New Landing Page';
        $this->form_validation->set_rules('general_promotional_text', 'General Promotional Text', 'required|trim|max_length[75]');
        $this->form_validation->set_rules('promotional_text', 'Promotional Text', 'required|trim|max_length[75]');
        $this->form_validation->set_rules('promotional_detail', 'Promotional Details', 'required|max_length[255]');
        $this->form_validation->set_rules('how_it_works', 'How it works', 'required|max_length[500]');
        $this->form_validation->set_rules('types_of_rewards', 'Types of Rewards', 'required|max_length[500]');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else {

            $upload_error = "";
            $image = $landing_page->image;

            $this->imaging->upload($_FILES["image"]);

            if ($this->imaging->uploaded == true) {

                $this->imaging->allowed = array('image/*');
                $this->imaging->file_new_name_body = smart_url(set_value('promotional_text'));
                $this->imaging->process('./images/');

                if ($this->imaging->processed == true) {
                    $image = $this->imaging->file_dst_name;
                } else {
                    $upload_error .= $this->imaging->error;
                }
            } else {
                $upload_error .= $this->imaging->error;
            }

            // build array for the model
            $form_data = array(
                'general_promotional_text' => set_value('general_promotional_text'),
                'promotional_text' => set_value('promotional_text'),
                'promotional_detail' => set_value('promotional_detail'),
                'image' => $image,
                'how_it_works' => set_value('how_it_works'),
                'types_of_rewards' => set_value('types_of_rewards'),
                'active' => set_value('active')
            );

            if ($this->landing_page_model->edit_landing_page($form_data, $page_id) == TRUE) {
                redirect('admin/manage_landing_pages');
            } else {
                redirect('admin/manage_landing_pages');
            }
        }
    }

    function delete_landing_page($page_id) {
        $this->landing_page_model->delete_landing_page($page_id);
        redirect('admin/manage_landing_pages');
    }

    function edit_offer($offer_id) {

        $data = array();
        $data['title'] = 'Edit Offer';
        $this->form_validation->set_rules('title', 'Title', 'required|trim|max_length[75]');
        $this->form_validation->set_rules('image', 'Image', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[255]');
        $this->form_validation->set_rules('url', 'Url', 'required|trim|max_length[255]');
        $this->form_validation->set_rules('active', 'Active', 'required|trim|max_length[4]');

        //get offer
        $data['offer'] = $offer = $this->offer_model->get_offer($offer_id);

        $data['active_options'] = array(
            '' => 'Please Select',
            '1' => 'Active',
            '0' => 'Not Active'
        );

        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');

        if ($this->form_validation->run() == FALSE) { // validation hasn't been passed
            $this->template->write_view('sidebar', 'admin/sidebar', $data);
            $this->template->write_view('content', $this->template_path . __FUNCTION__, $data);
            $this->template->render();
        } else { // passed validation proceed to post success logic
            $form_data = array(
                'category_id' => 1,
                'title' => set_value('title'),
                'image' => set_value('image'),
                'description' => set_value('description'),
                'url' => set_value('url'),
                'active' => set_value('active')
            );

            // run insert model to write data to db
            if ($this->offer_model->update_offer($form_data, $offer_id) == TRUE) { // the information has therefore been successfully saved in the db
                redirect('admin/manage_offers');
            } else {
                redirect('admin/manage_offers');
            }
        }
    }

    function delete_offer($offer_id) {
        if ($this->offer_model->delete_offer($offer_id) == TRUE) { // the information has therefore been successfully deleted from the db
            redirect('admin/manage_offers');
        } else {
            redirect('admin/manage_offers');
        }
    }

}

?>
