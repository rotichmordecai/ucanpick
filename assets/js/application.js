/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* Table initialisation */
$(document).ready(function() {
    
    $('input, textarea').placeholder();
    
    if (!window.location.origin) window.location.origin = window.location.protocol+"//"+window.location.host;
    
    var site_url = function($url_segments) { 
        return window.location.origin+'/'+$url_segments;  
    };
    
    $("a.click_offer").click(function() {
        var url = $(this).attr("url");
        window.open(url);
        //alert("Handler for .click() called.");
        return false;
    });
    
    
    $('img[class*=photo_middle]').each(function() {
        //get the width of the parent  
        var parent_height = $(this).parent().height();  
        //get the width of the image  
        var image_height = $(this).height()!=0 ? $(this).height() : 120;  
        //calculate how far from top the image should be  
        var top_margin = (parent_height - image_height)/2;  
        //and change the margin-top css attribute of the image  
        $(this).css( 'margin-top' , top_margin);
    });
    
    $('#gallery a').lightBox();
    
    if($.trim($("div#messages").html()).length != 0) {
        //$().toastmessage({sticky : true});
        $().toastmessage('showErrorToast', $("div#messages").html());
    }
    
    $('#manage_pages').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "sAjaxSource": site_url("admin/json_pages"),
        "sDom": "<'row'<'span4'l><'span4'f>r>t<'row'<'span4'i><'span4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        'aoColumns'      : 
        [
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        }
        ]
    } );
    
    $('#manage_landing_pages').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "sAjaxSource": site_url("admin/json_landing_pages"),
        "sDom": "<'row'<'span4'l><'span4'f>r>t<'row'<'span4'i><'span4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        'aoColumns'      : 
        [
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        }
        ]
    } );
    
    $('#manage_gifts').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "sAjaxSource": site_url("admin/json_gifts"),
        "sDom": "<'row'<'span4'l><'span4'f>r>t<'row'<'span4'i><'span4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        'aoColumns'      : 
        [
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        }
        ]
    } );
    
    $('#manage_offers').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "sAjaxSource": site_url("admin/json_offers"),
        "sDom": "<'row'<'span4'l><'span4'f>r>t<'row'<'span4'i><'span4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        'aoColumns'      : 
        [
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        }
        ]
    } );
    
    
    $('#manage_users').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "sAjaxSource": site_url("admin/json_users"),
        "sDom": "<'row'<'span4'l><'span4'f>r>t<'row'<'span4'i><'span4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        'aoColumns'      : 
        [
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : false
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        },
        {
            'bSearchable': true,
            'bVisible'   : true
        }
        ]
    } );
    
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url : site_url('assets/js/tiny_mce/tiny_mce.js'),

        // General options
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });

    
} );



