-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2012 at 10:35 AM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucanpick`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `description`, `active`) VALUES
(1, 'All', 'All', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('04b7eadb95b47af875d6609b210f3a7c', '207.237.211.201', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349402460, ''),
('0b8168a0557b5d318853e3530930ab6b', '212.92.202.48', '0', 1349377382, ''),
('161c036f51639aae78d5e1e8fbb25dd7', '66.249.73.104', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349422668, ''),
('163f85d152b222faf3a23940b6407f77', '174.252.19.253', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 S', 1349396668, ''),
('1ec44d8012e7f7842de451c21a3784a3', '66.249.73.80', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349422325, ''),
('1f0db0dcc4551303d16caa939ae97641', '66.249.73.85', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349422143, ''),
('1f132eda8a423c304e4992a0e8748207', '66.249.73.106', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349421608, ''),
('2003946dd4f3071a56c36e29c817c2a0', '66.249.73.228', 'Mediapartners-Google', 1349384496, 'a:2:{s:9:"user_data";s:0:"";s:17:"flash:new:message";s:38:"Kindly fill all Fields as you sign up!";}'),
('2d82ed87a0a6eb6a4c34a5572c47ec0c', '66.249.73.240', 'Mediapartners-Google', 1349384414, ''),
('39aa6a5ae56c72dd012beaec20d44cdd', '197.237.37.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349379811, 'a:4:{s:9:"user_data";s:0:"";s:7:"user_id";s:1:"1";s:8:"username";s:0:"";s:6:"status";s:1:"1";}'),
('4034cb3a038eae9720f855110f8ee4e8', '197.237.37.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349418534, 'a:2:{s:9:"user_data";s:0:"";s:17:"flash:old:message";s:21:"Error : Invalid login";}'),
('4b5249bb0c43453574aaa0442cbb4aeb', '197.237.37.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349383974, ''),
('4e064b62c9bba90edcee9549183fe438', '66.249.73.252', 'Mediapartners-Google', 1349384415, ''),
('56cb0e8fa55cf8887a9259596d672116', '66.249.73.233', 'Mediapartners-Google', 1349384414, ''),
('5cd432ccec196e7a6b41f2e8dc7b47f8', '66.249.73.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349422143, ''),
('61468a7db8a54f2265397e25ea648b7c', '66.249.73.215', 'Mediapartners-Google', 1349384415, ''),
('647470ebe9ae80f8a9432ad6268599c8', '197.237.37.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349418538, ''),
('6ce63745337afe0b25af31c31c29e3d1', '66.249.73.237', 'Mediapartners-Google', 1349384414, ''),
('7b2d7fc8bd42ade2bc7761abe82ab4c6', '66.249.73.236', 'Mediapartners-Google', 1349384292, ''),
('840950444df1f5f8cd74323ba8990be2', '212.92.202.48', '0', 1349382066, ''),
('8c84efd2e8f21a23e1edc9c64da14f96', '184.173.183.171', 'AddThis.com robot tech.support@clearspring.com', 1349414957, 'a:2:{s:9:"user_data";s:0:"";s:17:"flash:old:message";s:21:"Error : Invalid login";}'),
('b438e190630c22571f13c75353b2aaed', '212.92.202.48', '0', 1349391293, ''),
('b43d3333a6a40579ac2d53762591109a', '197.237.37.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349433204, ''),
('bc013781a9447b550a99c4e2d7298562', '66.249.73.237', 'Mediapartners-Google', 1349384415, ''),
('be1d1180a6a1f1be8dc9cbb82a0452d1', '212.92.202.48', '0', 1349396041, ''),
('c0762f7e1d7d963ada1c94ed7f79f9f6', '212.92.202.48', '0', 1349386584, ''),
('c0d622f70e53b282735dbf909c1a8e10', '66.249.73.220', 'Mediapartners-Google', 1349384415, ''),
('d9539de45b427d1d5b392f46632e90bf', '197.237.37.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349418534, 'a:2:{s:9:"user_data";s:0:"";s:17:"flash:old:message";s:21:"Error : Invalid login";}'),
('e5eb6f0680cf37f48293a0c3bfbcf9e3', '66.249.73.71', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349426435, 'a:2:{s:9:"user_data";s:0:"";s:17:"flash:new:message";s:38:"Kindly fill all Fields as you sign up!";}'),
('ea67aed00ca1b63df7cfef27ef7b3027', '212.92.202.48', '0', 1349400955, ''),
('f098ace7839ca4f1f69d7b43b9fbb928', '207.237.211.201', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4', 1349394196, ''),
('fa4cfa5c6414e9158d493f1267c2843b', '66.249.73.97', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349422979, ''),
('fac106cf0d53c76241a0943d34f2f879', '66.249.73.89', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349422473, ''),
('fbce6754ef118e3650008c033d818a40', '66.249.73.83', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1349424707, '');

-- --------------------------------------------------------

--
-- Table structure for table `gift`
--

CREATE TABLE IF NOT EXISTS `gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `image` varchar(75) DEFAULT NULL,
  `description` varchar(450) DEFAULT NULL,
  `referrals` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gift`
--

INSERT INTO `gift` (`id`, `name`, `image`, `description`, `referrals`, `active`) VALUES
(1, 'Ipod', 'Ipod_1.jpeg', 'Ipod', 7, 1),
(2, 'Ipad', 'Ipad_1.jpeg', 'Ipad', 8, 1),
(3, 'IPAD 2 16GB', 'IPAD216GB.jpg', 'Apple&amp;#39;s 2nd generation iPad has a 9.7-inch (diagonal) LED-backlit glossy widescreen and Multi-Touch display with IPS technology.\nAvailable in Black or White. \n', 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempt`
--

CREATE TABLE IF NOT EXISTS `login_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE IF NOT EXISTS `offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  `image` varchar(75) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offer_category1_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `category_id`, `title`, `image`, `description`, `url`, `created`, `active`) VALUES
(2, 1, 'Discover Student Open Road Card', 'http://content.linkoffers.net/SharedImages/Products/128/156.jpg', 'Discover Student Open Road Card', 'http://track.linkoffers.net/a.aspx?foid=14321348&amp;fot=9999&amp;foc=2&amp;foc2=156', NULL, 1),
(3, 1, 'IberiaBank Visa® Classic Card', 'http://content.linkoffers.net/SharedImages/Products/182/882.jpg', 'IberiaBank Visa® Classic Card', 'http://track.linkoffers.net/a.aspx?foid=14321401&amp;fot=9999&amp;foc=2&amp;foc2=882', NULL, 1),
(4, 1, 'Get a $25 Account Bonus when you sign up for Betterment.', 'http://content.linkoffers.net/SharedImages/Products/163455/523595.gif', 'Get a $25 Account Bonus when you sign up for Betterment.', 'http://track.linkoffers.net/a.aspx?foid=14364568&amp;fot=9999&amp;foc=2&amp;foc2=523595', NULL, 1),
(5, 1, 'EhealthInsurance', 'http://content.linkoffers.net/SharedImages/Products/3580/345901.gif', 'EhealthInsurance ', 'http://track.linkoffers.net/a.aspx?foid=14398967&amp;fot=9999&amp;foc=2&amp;foc2=345901', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(20000) DEFAULT NULL,
  `slug` varchar(75) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `description`, `slug`, `active`) VALUES
(1, 'About Us', '<p>At U Can Pick, our business is simple. Thousands of advertisers on the internet pay sites like us to generate leads to specific product offerings.&nbsp; Every time we do, we get paid a commission.&nbsp; At U Can Pick, we share that commission with our members by allowing you to earn cash or gifts for completing offers and getting others to do the same. &nbsp;Here&rsquo;s how it works:</p>\n<p>&nbsp;</p>\n<p>Sign up to become a member.</p>\n<p>Sign in to the site and select the gift you would like to receive.&nbsp; It could be cash, an IPAD or a Kindle&hellip;or you can email us with a custom gift request&hellip;U Can Pick!</p>\n<p>Pick one of our select offers (Companies like Discover Card, Iberia Bank, Ally Bank&hellip;) and sign up. &nbsp;&nbsp;We are very selective about the advertisers we partner with to ensure you have a positive experience.&nbsp; Most of the offers we have are for free trials or other products where you don&rsquo;t have to pay ANYTHING upfront&hellip;the other ones may have some fees or memberships involved. We will do our best to state which ones do involve fees to join.</p>\n<p>Once you sign up, get your friends involved&hellip;we give u a link you can share anywhere&hellip;facebook, twitter, a blog, your email list. &nbsp;Be creative! Each gift has a specific number of referrals needed in order to receive it.</p>\n<p>Once you get enough friends to sign up for an account and complete an offer on our site, you are eligible for your gift!</p>\n<p>&nbsp;</p>\n<p>We don&rsquo;t share your information with ANYONE and we only select offers from the most reputable companies.&nbsp; We want your experience with us to be a good one so you spread the word! Also, please feel free to contact us with any questions, comments, or concerns at&nbsp;<a href="mailto:info@ucanpick.com">info@ucanpick.com</a>.</p>', 'about', 1),
(2, 'FAQs', '<p><strong>This sounds too good to be true&hellip;how does it work?</strong></p>\n<p>Thousands of advertisers on the internet pay sites like us to generate leads to specific product offerings they have.&nbsp; Every time we do, we get paid a commission. We use those commissions to pay for the cash, IPads or Kindles our members receive.</p>\n<p>&nbsp;</p>\n<p><strong>How long after I sign up for an offer does it show up in my account?</strong></p>\n<p>It really depends on how long it takes the advertiser to notify us that the offer has been completed satisfactorily. It generally takes about 48-72 hours.&nbsp; If you haven&rsquo;t received credit, email us at <a href="mailto:info@ucanpick.com">info@ucanpick.com</a> and we&rsquo;ll look into it right away.</p>\n<p>&nbsp;</p>\n<p><strong>How long does it take to get my reward?</strong></p>\n<p>We send our rewards on the 15<sup>th</sup> of the month AFTER you get your last needed referral. So if your last referral gets credited on September 20<sup>th</sup>, your reward will be sent out on October 15<sup>th</sup>.</p>\n<p>&nbsp;</p>\n<p><strong>Can I sign up for more than one offer?</strong></p>\n<p>You can&rsquo;t sign up for more than one offer at a time. Once you complete the requirements for a gift, you sign up for another offer and start the process again to get a new gift.</p>\n<p>&nbsp;</p>\n<p>Any other questions? Email us at <a href="mailto:info@ucanpick.com">info@ucanpick.com</a>.</p>', 'faqs', 1),
(3, 'Testimonials', '<p>See What Our Members Are Saying...</p>\n<p>&nbsp;</p>\n<p>"I was definitely skeptical when I saw the site...so I started small and decided to go for the $50. &nbsp;It worked exactly as promised! I signed up for an offer and got 2 friends to sign up as well. The next month, I got a check in the mail for $50. Now I''m trying for my next reward." - Diane C, Fort Myers, FL.&nbsp;</p>\n<p>&nbsp;</p>\n<p>"Got my Kindle last week! Thanks U Can Pick!" - Dave S, New York, NY.</p>\n<p>&nbsp;</p>\n<p>"Just received my $100 check. So awesome and customer service was really helpful" - Mike H., Stamford, NY.</p>\n<p>&nbsp;</p>', 'testimonials', 1),
(4, 'Contact Us', '<p>We pride ourselves on being responsive to any and all customer requests and will get back to you within 24 hours of your message. &nbsp;Our goal is to make sure you have a great experience with us.&nbsp;</p>\n<p>&nbsp;</p>\n<p>General questions or comments: <a href="mailto:info@ucanpick.com">info@ucanpick.com</a></p>\n<p>Technical Support: <a href="mailto:support@ucanpick.com">support@ucanpick.com</a></p>\n<p>PR or Marketing Related Inquires: <a href="mailto:pr@ucanpick.com">pr@ucanpick.com</a></p>\n<p>&nbsp;</p>', 'contactus', 1),
(5, 'Terms &amp; Conditions', '<p>Mattis diam? Massa duis eros augue habitasse massa? Nisi nisi? Etiam rhoncus nascetur. Tortor porta nunc dignissim nec, eros, integer? Tristique pulvinar! Arcu rhoncus enim, in urna nascetur enim vel? Nisi tortor, nec in parturient porttitor. Sociis duis a pid montes etiam proin nec tortor? In odio magnis, pulvinar augue ut ridiculus pellentesque vel! In, lundium! Porttitor! Placerat, placerat turpis augue aliquet tincidunt lacus! Nunc etiam aliquet, mid, ac eu! Amet dictumst rhoncus pulvinar rhoncus sociis! Magna cursus risus integer! Tincidunt etiam et lorem, augue, ut nisi. Ultricies eu sit, placerat, adipiscing vel nisi ut mus, nisi mus odio. Porttitor, magnis a et velit diam! Dolor massa lorem aenean dapibus ac dictumst, placerat augue velit aliquet non adipiscing placerat, tortor.</p>\n<p>Phasellus duis integer dis, duis. Phasellus lacus turpis nunc sociis, quis in! Porta mauris et, adipiscing platea, sit ac! Et scelerisque a duis. Augue nunc urna nec, parturient pulvinar pulvinar eu natoque ac mid augue magna ac mauris integer enim tincidunt ultricies natoque! Et, porttitor dis scelerisque! A in proin diam cras in ultrices nisi nunc dictumst velit montes, mauris lorem odio, vut porta ultricies parturient ultricies amet aenean. Pulvinar augue? Nunc vut pulvinar, elementum montes hac. Dictumst porttitor? Ac mid lundium natoque nisi pulvinar! Dolor vel? Magna pulvinar? Elementum ultrices montes odio amet lundium, phasellus ac est rhoncus. Velit a adipiscing turpis, sit platea, ut? Mattis placerat cursus! Nunc amet magna vut, nisi urna dictumst quis platea amet.</p>\n<p>Mattis diam? Massa duis eros augue habitasse massa? Nisi nisi? Etiam rhoncus nascetur. Tortor porta nunc dignissim nec, eros, integer? Tristique pulvinar! Arcu rhoncus enim, in urna nascetur enim vel? Nisi tortor, nec in parturient porttitor. Sociis duis a pid montes etiam proin nec tortor? In odio magnis, pulvinar augue ut ridiculus pellentesque vel! In, lundium! Porttitor! Placerat, placerat turpis augue aliquet tincidunt lacus! Nunc etiam aliquet, mid, ac eu! Amet dictumst rhoncus pulvinar rhoncus sociis! Magna cursus risus integer! Tincidunt etiam et lorem, augue, ut nisi. Ultricies eu sit, placerat, adipiscing vel nisi ut mus, nisi mus odio. Porttitor, magnis a et velit diam! Dolor massa lorem aenean dapibus ac dictumst, placerat augue velit aliquet non adipiscing placerat, tortor.</p>\n<p>Phasellus duis integer dis, duis. Phasellus lacus turpis nunc sociis, quis in! Porta mauris et, adipiscing platea, sit ac! Et scelerisque a duis. Augue nunc urna nec, parturient pulvinar pulvinar eu natoque ac mid augue magna ac mauris integer enim tincidunt ultricies natoque! Et, porttitor dis scelerisque! A in proin diam cras in ultrices nisi nunc dictumst velit montes, mauris lorem odio, vut porta ultricies parturient ultricies amet aenean. Pulvinar augue? Nunc vut pulvinar, elementum montes hac. Dictumst porttitor? Ac mid lundium natoque nisi pulvinar! Dolor vel? Magna pulvinar? Elementum ultrices montes odio amet lundium, phasellus ac est rhoncus. Velit a adipiscing turpis, sit platea, ut? Mattis placerat cursus! Nunc amet magna vut, nisi urna dictumst quis platea amet.</p>\n<p>Mattis diam? Massa duis eros augue habitasse massa? Nisi nisi? Etiam rhoncus nascetur. Tortor porta nunc dignissim nec, eros, integer? Tristique pulvinar! Arcu rhoncus enim, in urna nascetur enim vel? Nisi tortor, nec in parturient porttitor. Sociis duis a pid montes etiam proin nec tortor? In odio magnis, pulvinar augue ut ridiculus pellentesque vel! In, lundium! Porttitor! Placerat, placerat turpis augue aliquet tincidunt lacus! Nunc etiam aliquet, mid, ac eu! Amet dictumst rhoncus pulvinar rhoncus sociis! Magna cursus risus integer! Tincidunt etiam et lorem, augue, ut nisi. Ultricies eu sit, placerat, adipiscing vel nisi ut mus, nisi mus odio. Porttitor, magnis a et velit diam! Dolor massa lorem aenean dapibus ac dictumst, placerat augue velit aliquet non adipiscing placerat, tortor.</p>\n<p>Phasellus duis integer dis, duis. Phasellus lacus turpis nunc sociis, quis in! Porta mauris et, adipiscing platea, sit ac! Et scelerisque a duis. Augue nunc urna nec, parturient pulvinar pulvinar eu natoque ac mid augue magna ac mauris integer enim tincidunt ultricies natoque! Et, porttitor dis scelerisque! A in proin diam cras in ultrices nisi nunc dictumst velit montes, mauris lorem odio, vut porta ultricies parturient ultricies amet aenean. Pulvinar augue? Nunc vut pulvinar, elementum montes hac. Dictumst porttitor? Ac mid lundium natoque nisi pulvinar! Dolor vel? Magna pulvinar? Elementum ultrices montes odio amet lundium, phasellus ac est rhoncus. Velit a adipiscing turpis, sit platea, ut? Mattis placerat cursus! Nunc amet magna vut, nisi urna dictumst quis platea amet.</p>', 'terms', 1),
(6, 'Privacy Policy', 'Eros purus duis odio magna rhoncus tortor magna elit nascetur phasellus turpis, scelerisque in ridiculus? Natoque porta sit? Risus turpis, nisi! Porta integer amet dignissim vel mus? Aenean placerat ac lectus et etiam amet ut mattis nisi adipiscing dapibus, diam enim tincidunt magna proin parturient amet scelerisque sagittis sit mus, magna! Nisi, cursus aenean, nascetur elementum. Tortor! Augue penatibus non, urna pellentesque risus, magna integer, elementum? Rhoncus lectus, amet, dapibus? Placerat placerat, in magna porta! Ultrices, nunc in phasellus proin, sagittis, parturient, nunc nunc amet, platea. Et elementum, nunc dapibus magnis auctor. Quis nec habitasse et, natoque dapibus, parturient urna dis velit rhoncus nisi? Pid, a odio nec? Mauris dictumst, dignissim, pulvinar nisi arcu pid, ultrices et rhoncus risus augue.', 'privacy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `referrer_gift`
--

CREATE TABLE IF NOT EXISTS `referrer_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referrer` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_referral_gift_user1_idx` (`user_id`),
  KEY `fk_referral_gift_gift1_idx` (`gift_id`),
  KEY `fk_referral_gift_user2_idx` (`referrer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `referrer_gift`
--

INSERT INTO `referrer_gift` (`id`, `referrer`, `user_id`, `gift_id`, `status`) VALUES
(1, 1, 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `created` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, '', '$P$B8As59vYnrLNxnIIGKRpWp84sEACiU1', 'korir.mordecai@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '197.237.37.79', '2012-10-04 19:24:03', '2012-09-30 15:24:47', '2012-10-04 19:24:03'),
(2, '', '$2a$08$ZDNIMrwqFv.AOwrwxZ0ZU.nYSQ.uWq/E41cL75ql.JPvIWzMNmUDu', 'rotichmordecai@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2012-10-01 06:06:48', '2012-10-01 06:06:15', '2012-10-01 14:06:48'),
(3, '', '$P$Blxklm.FO84KNKH3999dVjzBF22wGL0', 'joeanto@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '207.237.211.201', '2012-10-01 09:06:42', '2012-10-01 09:03:18', '2012-10-01 14:06:42'),
(4, '', '$2a$08$KBjwUwOXGlqyehXN9I.VgO7q4GolGmC8nZ.1SNALPyntOLnvX2Rh2', 'joe@rumbatime.com', 0, 0, NULL, NULL, NULL, NULL, NULL, '207.237.211.201', '2012-10-04 12:08:45', '2012-10-04 11:21:30', '2012-10-04 12:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`),
  KEY `fk_user_autologin_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_gift`
--

CREATE TABLE IF NOT EXISTS `user_has_gift` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `success` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_has_gift_gift1_idx` (`gift_id`),
  KEY `fk_user_has_gift_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_has_gift`
--

INSERT INTO `user_has_gift` (`id`, `user_id`, `gift_id`, `success`) VALUES
(1, 1, 1, 0),
(2, 2, 1, 0),
(3, 3, 1, 0),
(4, 4, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_offer`
--

CREATE TABLE IF NOT EXISTS `user_has_offer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `gift_id` int(11) DEFAULT NULL,
  `success` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_has_offer_gift1_idx` (`gift_id`),
  KEY `fk_user_has_offer_user1_idx` (`user_id`),
  KEY `fk_user_has_offer_offer1_idx` (`offer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_has_offer`
--

INSERT INTO `user_has_offer` (`id`, `user_id`, `offer_id`, `gift_id`, `success`, `created`) VALUES
(1, 1, 2, 1, 0, '2012-09-30 16:23:59'),
(2, 1, 3, 1, 0, '2012-09-30 16:24:23'),
(3, 2, 3, 1, 1, '2012-10-01 06:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(145) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `postal_code` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(75) COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `referrer` int(11) NOT NULL,
  `refer_status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_user_profile_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `is_admin`, `name`, `address`, `city`, `postal_code`, `county`, `country`, `referrer`, `refer_status`) VALUES
(1, 1, 0, 'Rotich Mordecai', 'Box 1900', 'Kitale', '30200', 'Rift Valley', 'Kenya', 0, 1),
(2, 2, 0, 'Rotich Mordecai', NULL, NULL, NULL, NULL, NULL, 1, 1),
(3, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `fk_offer_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD CONSTRAINT `fk_user_autologin_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_has_gift`
--
ALTER TABLE `user_has_gift`
  ADD CONSTRAINT `fk_user_has_gift_gift1` FOREIGN KEY (`gift_id`) REFERENCES `gift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_gift_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_has_offer`
--
ALTER TABLE `user_has_offer`
  ADD CONSTRAINT `fk_user_has_offer_gift1` FOREIGN KEY (`gift_id`) REFERENCES `gift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_offer_offer1` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_has_offer_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `fk_user_profile_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
